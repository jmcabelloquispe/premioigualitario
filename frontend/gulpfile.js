'use strict';

var gulp       = require('gulp'),
    changed    = require('gulp-changed' ),
    concat     = require('gulp-concat' ),
    gutil      = require('gulp-util'),
    pug        = require('gulp-pug'),
    jshint     = require('gulp-jshint'),
    plumber    = require('gulp-plumber'),
    rename     = require('gulp-rename'),
    sass       = require('gulp-sass'),
    size       = require('gulp-size'),
    uglify     = require('gulp-uglify' ),
    webpack    = require('webpack-stream'),
    prefix     = require('gulp-autoprefixer');
    // babel      = require('gulp-babel');

var env = process.env.NODE_ENV || 'development',

    /* Solo activar con Django */
    outputDir = '../project/static/',
    outputDirTemplate = '../project/templates';

function onError (error) {
    gutil.log(error);
    this.emit('end');
}

/* Transpilar Pug a HTML */
gulp.task('templates', function () {
    return gulp.src('source/layout/*.pug')
        .pipe( plumber({ errorHandler: onError }) )
        .pipe( changed(outputDirTemplate) )
        .pipe( pug({
            pretty: env === 'development'
        }) )
        .pipe( gulp.dest(outputDirTemplate) );
});


/* Linter a Javascript */
gulp.task('js', function () {
    return gulp.src('source/javascript/application.js')
        .pipe( jshint() )
        .pipe( jshint.reporter( require('jshint-stylish') ) )
        .pipe( plumber({ errorHandler: onError }) )
        .pipe( webpack({
            watch: true,
            output: {
                filename: 'bundle.js'
            }
        }) )
        //.pipe( uglify() )
        // .pipe(babel({
        //     presets: ['env']
        // }))
        .pipe( size() )
        .pipe( gulp.dest(outputDir + '/js') );
});


gulp.task('vendor', function() {
    return gulp.src( './source/javascript/plugins/*.js' )
        .pipe( concat('vendor_all.js') )
        .pipe( uglify() )
        .pipe( size() )
        .pipe( gulp.dest( outputDir + '/js/vendor') );
});


/* Transpilar SaSS a CSS */
gulp.task('styles', function () {
    var config = {
        outputStyle : 'compressed'
    };

    return gulp.src('source/sass/style.scss')
        .pipe( plumber({ errorHandler: onError }) )
        .pipe( changed(outputDir + '/css') )
        .pipe( sass() )
        .pipe( gulp.dest(outputDir + '/css') )
        .pipe( sass(config) )
        .pipe( rename('style.min.css') )
        .pipe(prefix({browsers: ['last 2 versions', 'ie 8', 'ie 9', '> 1%', 'Firefox >= 20', 'Opera 12.1','iOS 7'], cascade: false}))
        .pipe( size() )
        .pipe( gulp.dest(outputDir + '/css') );
});


gulp.task('watch', function () {
    gulp.watch( 'source/layout/**/*.pug', ['templates'] );
    gulp.watch( 'source/javascript/**/*.js', ['js'] );
    gulp.watch( 'source/javascript/plugins/*.js', ['vendor'] );
    gulp.watch( 'source/sass/**/*.scss', ['styles'] );
});



var taskBuild = [
    'templates',
    'js',
    'vendor',
    'styles'
];

var taskDefault = [
    'templates',
    'js',
    'vendor',
    'styles',
    'watch'
];


gulp.task('build', taskBuild);

gulp.task('default', taskDefault );
