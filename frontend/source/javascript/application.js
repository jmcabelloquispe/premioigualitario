"use strict";
// Componentes
var Feature = require('./components/features.js');
var FormCase = require('./components/formcase.js');
var Participant = require('./components/participant.js');
var ParticipantDetail = require('./components/participantdetail.js');
var FinalParticipant = require('./components/finalparticipant.js');
var FinalParticipantDetail = require('./components/finalparticipantdetail.js');
var ResetPassword = require('./components/resetpassword');
var Login = require('./components/login.js');
var Common = require('./components/common.js');
var APP, UTIL;

APP = {

    /* Funciones y mÃƒÅ todos que se aplican al site en general */
    common: {
        init: function() {
            new Common();
        }
    },

    home: {
        init: function() {
            new Feature();
        }
    },

    formcase: {
        init: function() {
            new FormCase();
        }
    },

    participant: {
        init: function() {
            new Participant();
        }
    },

    participantdetail: {
        init: function() {
            new ParticipantDetail();
        }
    },

    finalparticipant: {
        init: function() {
            new FinalParticipant();
        }
    },

    finalparticipantdetail: {
        init: function() {
            new FinalParticipantDetail();
        }
    },

    resetpassword: {
        init: function() {
            new ResetPassword();
        }
    },

    login: {
        init: function() {
            new Login();
        }
    }
};


/* Ejecutador de metodos segÃºn vista */

UTIL = {
    exec: function(controller, action) {
        var ns = APP;

        action = (action === undefined ? "init" : action);

        if (controller !== "" && ns[controller] && typeof ns[controller][action] === "function") {
            ns[controller][action]();
        }
    },

    init: function() {
        var controller = document.getElementsByTagName('body')[0].dataset.controller;

        UTIL.exec("common");
        UTIL.exec(controller);
    }
};

$(window).on('load', UTIL.init);