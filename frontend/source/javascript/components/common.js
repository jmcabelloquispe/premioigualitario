var Common = function(){
    this.initialize();
};

Common.prototype.initialize = function(){
    this.handlers();
};

Common.prototype.handlers = function(){
    var self = this;
    $('a.facebook').on('click', function(){
        ga('send', 'social', 'Facebook', 'share', 'La Hora Inca Kola 2019');
        FB.ui({
            method: 'feed',
            link: 'https://www.facebook.com/IncaKola/videos/300957967260541/',
            caption: 'DiaInternacionaldelasMujeres'
        }, function(response){

        });
    });

    $('a.twitter').on('click', function(){
        ga('send', 'social', 'Twitter', 'tweet', 'La Hora Inca Kola 2019');
       var link = $(this).data('link');
       var message = $(this).data('message');
       var hashtag = 'LaHoraIncaKola,DiaDeLaMujer'
        uri = 'https://twitter.com/intent/tweet?url=' + link + '&text=' +
              encodeURI(message) + ' &hashtags=' + hashtag + '&display=popup';
        window.open(
            uri,
            "",
            "status = 1, height = 450, width = 620, resizable = 0"
        );
    });
};

module.exports = Common;