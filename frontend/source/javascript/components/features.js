var Feature = function(){
    this.initialize();
    this.onPageReady();
};

Feature.prototype.initialize = function(){
    this.handlers();
};

Feature.prototype.handlers = function(){

    var flag = true;
    
    $('.js_slick_statistic').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 980,
                settings: 'unslick'
            }
        ]
    }); 

    $(window).on('resize', function() {
        $('.js_slick_statistic').slick('resize');
    });

    $('.js_slick_news').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        dots: true,
        responsive: [           
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
    });

    $('.js_slick_committee').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        dots: true,
        responsive: [           
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
    });

    $('.js_slick_evaluator').slick({
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: false,
      dots: true,
      responsive: [           
          {
            breakpoint: 769,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
    });

    $('.js_slick_galery').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true
    });

    $('.js_option_tab').on("click", function(){
      $('.js_option_tab').removeClass('option--active');
      $(this).addClass('option--active');
      $('.js_committee, .js_evaluator').css("display", "none");
      var tab = "."+$(this).data("tab");
      var carousel = "."+$(this).data("carousel");   
      $('.js_tab_one, .js_tab_two').removeClass('tab--active');
      $(tab).addClass('tab--active');
      $(carousel).slick('setPosition', 0);
      $(carousel).slick("refresh");
      var itemPos = $(this).position();
      $(".tabs_options .selector").css({
        "left":itemPos.left + "px", 
      });
    });

    $('.js_open_modal').on('click', function(){
      Swal.fire({
        title: $(this).data("title"),
        text: $(this).data("message"),
        showConfirmButton: false,
        showCloseButton: true,
      })
    });

    // hace scroll hasta la seccion selecionada en version desktop
    $('.js_option_header').on('click', function(){
      var section = "#"+$(this).data('section');
      // window.location.hash = section;
      $('html, body').animate({
        scrollTop: $(section).offset().top - 77
      }, 1000)
    });
    
    // despliega o oculta menu en version mobile
    $('.js_hamburger').on('click', function(){                 
      document.body.classList.toggle('withoutScroll');
      if(!flag){        
        $('.js_menu_mobile').removeClass('menu_mobile--show');          
        $(this).removeClass('rotate');  
        $('.js_menu_opacity').removeClass('menu_opacity--show');              
        flag = true;
      }else {               
        $('.js_menu_mobile').addClass('menu_mobile--show');              
        $(this).addClass('rotate');  
        $('.js_menu_opacity').addClass('menu_opacity--show'); 
        flag = false;
      }
    });

    $('.js_option_header_mobile').on('click', function(){
      document.body.classList.toggle('withoutScroll');    
      var section = "#"+$(this).data('section');
      $('.js_menu_mobile').removeClass('menu_mobile--show');          
      $('.js_hamburger').removeClass('rotate');  
      $('.js_menu_opacity').removeClass('menu_opacity--show');
      // window.location.hash = section;
      $('html, body').animate({
        scrollTop: $(section).offset().top - 70
      }, 1000)                
      flag = true;
    });

    $('.js_arrow_header_mobile').on('click', function(){
      var section = "#"+$(this).data('section');
      $('.js_menu_mobile').removeClass('menu_mobile--show');
      $('.js_hamburger').removeClass('rotate');
      $('.js_menu_opacity').removeClass('menu_opacity--show');
      // window.location.hash = section;
      $('html, body').animate({
        scrollTop: $(section).offset().top - 70
      }, 1000)
      flag = true;
    });

    $(document).scroll(function() {
      var y = $(this).scrollTop();
      if (y > 30) {
        $('.js_header').addClass('header-fixed');
      } else {
        $('.js_header').removeClass('header-fixed');
      }
    });

    if(window.location.hash){
      if (window.innerWidth <= 768) {
        $('html, body').animate({
          scrollTop: $(window.location.hash).offset().top - 70
        }, 1000) 
      }else {
        $('html, body').animate({
          scrollTop: $(window.location.hash).offset().top
        }, 1000)
      }     
    }
};

Feature.prototype.onPageReady = function(){
    $('#loader').fadeOut('slow', function(e){
        $(this).remove();
    });
};

module.exports = Feature;