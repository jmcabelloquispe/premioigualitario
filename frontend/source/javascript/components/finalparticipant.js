var FinalParticipant = function(){
  this.initialize();
  this.activeFilter();
  this.onPageReady();

};

FinalParticipant.prototype.initialize = function(){
  var self = this;
  self.handlers();
};

FinalParticipant.prototype.handlers = function(){

    // filtra la lista de participantes por estado
    $('.js_filter_state, js_filter_category').on('click', function(){
        $('.js_filter_state').removeClass('option--active');
        $(this).addClass('option--active');
    });

    $('.js_hamburger').on('click', function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.js_menu').css('display', 'none');
        }else {
            $(this).addClass('active');
            $('.js_menu').css('display', 'flex');
        }
    });

    $('.js_select_state, .js_select_category').on('change', function () {
        window.location.search = $(this).find(':selected').data('href');
    })
};

FinalParticipant.prototype.activeFilter = function(){
    var url_string = window.location.href;
    var url = new URL(url_string);
    var state = url.searchParams.get("state");
    var category = url.searchParams.get("category");

    if(state){
        $('.js_filter_state').removeClass('option--active');
        $(".js_filter_state[data-state='"+ state +"']").addClass('option--active');
        $(".js_select_state").val(state);
        $('.js_filter_category').each(function() {
            $(this).attr("href","?state="+state+"&category="+$(this).data('category'));
        });
        $('.js_select_category > option').each(function () {
            $(this).data("href","?state="+state+"&category="+$(this).val());
        });
    }

    if(category){
        $('.js_filter_category').removeClass('option--active');
        $(".js_filter_category[data-category='"+ category +"']").addClass('option--active');
        $(".js_select_category").val(category);
        $('.js_filter_state').each(function() {
            $(this).attr("href","?category="+category+"&state="+$(this).data('state'));
        });
        $('.js_select_state > option').each(function() {
            $(this).data("href","?category="+category+"&state="+$(this).val());
        });
    }
};

FinalParticipant.prototype.onPageReady = function(){
    $('#loader').fadeOut('slow', function(e){
        $(this).remove();
    });
};
module.exports = FinalParticipant;