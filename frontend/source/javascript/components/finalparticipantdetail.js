var FinalParticipantDetail = function(){
  this.form;
  this.initialize();
  this.onPageReady();
};

FinalParticipantDetail.prototype.initialize = function(){
  var self = this;
  self.form = $("#form_participant_evaluation");
  self.validate();
  self.handlers();
  self.inputrules();
};

FinalParticipantDetail.prototype.validate = function(){
  var self  = this;
  self.form.validate({
    debug: false,
    errorClass: 'form-error',
    messages: {
      comment: {
        required: "Campo requerido",
        minlength: "El texto ingresado no es válido",
        maxlength: "El texto ingresado no es válido",
      }
    },
    rules: {
      comment: {
        required: true,
        minlength: 2,
        maxlength: 250,
      }
    },
    errorPlacement: function (error, element) {
        error.insertAfter(element);
    },
    highlight: function(element, errorClass, validClass) {
      element = $(element);
      element.addClass('form-error');
    },
    unhighlight: function (element, errorClass, validClass) {
      element = $(element);
      element.removeClass('form-error');
    }
  });
};

FinalParticipantDetail.prototype.handlers = function(){

    var self  = this;
    $('.js_range').on('input', function () {
        $(this).parent().find('.js_point').html($(this).val());
    });

    $("#send_form").on('click', function () {
        var isValid = self.form.valid();
        if(isValid){
            var formcase = document.getElementById('form_participant_evaluation');
          var formData = new FormData(formcase);
          Swal.fire({
            type: "warning",
            title: "Importante",
            html:"<p>Revisar la información antes de enviar</p>",
            showConfirmButton: true,
            confirmButtonText: "Continuar",
            customClass: "swal2-popup--medium",
            showCloseButton: true,
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
              return fetch('/api/business/company/final/evaluation/', {
                  method: 'post',
                  body: formData,
              }).then(response => {
                if (!response.ok) {
                  throw new Error(response.statusText)
                }
                return response.json()
              })
              .catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                )
              })
            },
            allowOutsideClick: () => !swal.isLoading()
          }).then((result) => {
            if (result.value) {
                window.location.href="/dashboard/final/participant/";
            }
          })
        }
    });

    $('.js_hamburger').on('click', function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.js_menu').css('display', 'none');
        }else {
            $(this).addClass('active');
            $('.js_menu').css('display', 'flex');
        }
    });
};

FinalParticipantDetail.prototype.inputrules = function(){

  $("#comment").alphanum({
    disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
    allow: '@._-'
  });

};

FinalParticipantDetail.prototype.onPageReady = function(){
    $('#loader').fadeOut('slow', function(e){
        $(this).remove();
    });
};
module.exports = FinalParticipantDetail;