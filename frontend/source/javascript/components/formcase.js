var FormCase = function(){
  this.form;
  this.initialize();
  this.onPageReady();
};

Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});

FormCase.prototype.initialize = function(){
  var self = this;
  self.form = $("#form_case");
  self.validate();
  self.handlers();
  self.inputrules();
};

FormCase.prototype.validate = function(){
  var self  = this;
  self.form.validate({    
    debug: false,
    errorClass: 'form-error',
    messages: {
      name: {
        required: "Campo requerido",
        minlength: "El texto ingresado no es válido",
        maxlength: "El texto ingresado no es válido",
      },
      ruc: {
        required: "Campo requerido",
        minlength: "El RUC ingresado no es válido",
        existruc: "El RUC ya se encuentra registrado en todas las categorías"
      },
      representative: {
        required: "Campo requerido",
        minlength: "El texto ingresado no es válido",
        maxlength: "El texto ingresado no es válido",
      },
      email: {
        required: "Campo requerido",
        minlength: "El correo ingresado no es válido",
        maxlength: "El correo ingresado no es válido",
        validEmail: "El correo ingresado no es válido",
      },
      phone: {
        required: "Campo requerido",
        minlength: "El teléfono ingresado no es válido",
        maxlength: "El teléfono ingresado no es válido",
      },
      terms: {
        required: "Debe aceptar los términos y condiciones",
      },
      videourl: {
        required: "Campo requerido",
        url: "La url ingresada no es válida",
      },
      pdf:{
        required: 'Campo requerido',
        filesize: 'El pdf ingresado excede 5MB',
        extension: 'El pdf ingresado no es válido'
      },
      photo:{
        required: 'Campo requerido',
        filesize: 'La foto ingresada excede 5MB',
        extension: 'La foto ingresada no es válido'
      },
      initiative: {
        required: "Campo requerido",
        maxlength: "El texto ingresado no es válido",
      },
      verifycheck: {
        required: "Campo requerido",
        minlength: "Campo requerido",
        repeatedcategory: "Ya te encuentras registrado en la categoría",
        exceedlimit: "El RUC ingresado ya excedió el numero máximo de categorías"
      }
    },
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 120,
      },
      ruc: {
        required: true,
        minlength: 11,
        existruc: true
      },
      representative: {
        required: true,
        minlength: 2,
        maxlength: 120,
      },
      email: {
        required: true,
        minlength: 6,
        maxlength: 120,
        // email: true,
        validEmail:true,
      },
      phone: {
        required: true,
        minlength: 7,
        maxlength: 9,
      },
      terms:{
        required: function(){
          $(".js_terms").is(':checked')
        }
      },
      videourl: {
        required: false,
        maxlength: 200,
        url: true
      },
      pdf:{
        required: false,        
        extension: "pdf",
        filesize: 5000000,
      },
      photo:{
        required: false,        
        extension: "jpg|jpeg|png",
        filesize: 5000000,
      },
      initiative: {
        required: true,
        maxlength: 250,
      },
      verifycheck: {
        required: true,
        minlength: 1,
        exceedlimit: true,
        repeatedcategory: true
      }
    },
    errorPlacement: function (error, element) {
      if(element.hasClass('js_terms')){
        error.insertAfter(element.parent().parent());
      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
        error.insertAfter(element.parent());
      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
        error.insertAfter(element.parent());
      }
      else {
        error.insertAfter(element);
      }
    },
    highlight: function(element, errorClass, validClass) {
      element = $(element);
      if(element.hasClass('js_terms')){
        element.parent().addClass('form-error');
        // element.parent().next().addClass('label-error');
      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
        element.parent().addClass('form-error');
      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
        element.parent().addClass('form-error');
      }
      else {
        element.addClass('form-error');
      }
    },
    unhighlight: function (element, errorClass, validClass) {
      element = $(element);
      if(element.hasClass('js_terms')){
        element.parent().removeClass('form-error');
        // element.parent().next().removeClass('label-error');
      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
        element.parent().removeClass('form-error');
      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
        element.parent().removeClass('form-error');
      }
      else {
        element.removeClass('form-error');
      }
    }
  });
}

FormCase.prototype.handlers = function(){
    var self  = this;
    // configuracion formulario de pasos del formulario
    self.form.steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "fade",
      autoFocus: true,
      labels: {
        prev: false,
        next: "Continuar",
        finish: "Enviar postulación"
      },
      onStepChanging: function (event, currentIndex, newIndex)
      {  
        if (currentIndex > newIndex)
        {
            return true;
        }
        self.form.validate().settings.ignore = ":disabled,:hidden";
        var isValid = self.form.valid();
        if(isValid){
          var formcase = document.getElementById('form_case');
          var formData = new FormData(formcase);
          fetch('/api/business/company/', {
            method: 'post',
            body: formData,
          }).then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          });
          // actualiza el estado de las opciones del menu
          if(newIndex===0){
            $('.js_step_next').removeClass('option--active');
            $('.js_step_previous').addClass('option--active');
          }else {
            $('.js_step_next').addClass('option--active');
            $('.js_step_previous').removeClass('option--active');
          }
        }
        return isValid;
      },
      onFinished: function (event, currentIndex)
      { 
        self.form.validate().settings.ignore = ":disabled";
        var isValid = self.form.valid();
        if(isValid){
          var formcase = document.getElementById('form_case');
          var formData = new FormData(formcase);
          // se agrega las preguntas al formdata
          $('.js_companyquestion_active').each(function(index, element){
            formData.append("questions["+index+"]question", $(this).data('companyquestion'));
            formData.append("questions["+index+"]response", $(this).val());
          });
          // muestra alerta antes de hacer el post
          Swal.fire({
            type: "warning",
            title: "Importante",
            html:"<p>Revisar la información antes de enviar</p>",
            showConfirmButton: true,
            confirmButtonText: "Continuar",
            customClass: "swal2-popup--medium",
            showCloseButton: true,
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
              return fetch('/api/business/company/step/two/', {
                  method: 'post',
                  body: formData,
              }).then(response => {
                if (!response.ok) {
                  throw new Error(response.statusText)
                }
                return response.json()
              })
              .catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                )
              })
            },
            allowOutsideClick: () => !swal.isLoading()
          }).then((result) => {
            if (result.value) {
              window.location.href="/thanks";
            }
          })
        }
        return isValid;
      }
    });

    // ejecuta el paso anterior en el formulario
    $('.js_step_previous').on('click', function(){      
      var current = self.form.steps("previous");
      if(current){
        $('.js_step_next').removeClass('option--active');
        $(this).addClass('option--active');
      }
    });

    // ejecuta el paso siguiente en el formulario
    $('.js_step_next').on('click', function(){      
      var current = self.form.steps("next");
      if(current){
        $('.js_step_previous').removeClass('option--active');
        $(this).addClass('option--active');
      }
    });

    // se aplica jcf a los inputs checkbox y file
    jcf.replaceAll();

    // contador de caracteres por input textarea
    $('textarea').on('keyup', function() {
       $(this).parent().find('.count .number').html(700 - $(this).val().length) ;
    });

    // agregar nuevo input url de video
    $(document).on('click', '.js_add_input_url', function(){
      var content = $("body").find(".js_input_url").parent().clone();
      content.find("input").removeClass("js_input_url");      
      content.find("input").removeClass("form-error");  
      content.find("label").remove();
      content.find("input").addClass("js_input_url_clone"); 
      content.find("input").attr('name', 'videourl_additional'); 
      content.find("input").attr('id', 'videourl_additional');
      content.find("input").val('');
      content.insertAfter($(".js_input_url").parent());
      $(content.find("input")).rules("add", {
        required: true,
        url: true,
        messages: {
          required: "Campo requerido",
          url: "La url ingresada no es válida"
        }
      });
      $(this).removeClass("js_add_input_url").addClass('js_remove_input_url');
      $(this).removeClass("button--add").addClass('button--remove');
      $(this).find('.text').text('Quitar segunda URL');
   });

    // eliminar input url de video
    $(document).on('click', '.js_remove_input_url', function(){
      $("body").find(".js_input_url_clone").parent().remove();
      $(this).removeClass("js_remove_input_url").addClass('js_add_input_url');
      $(this).removeClass("button--remove").addClass('button--add');
      $(this).find('.text').text('Añadir otra URL de video');
    });

     // agregar nuevo input url de pdf
     $(document).on('click', '.js_add_input_pdf', function(){
      var content = $("body").find(".js_input_pdf").closest(".content_input").clone();
      content.append(content.find('input'));
      content.find('.jcf-file').remove();
      content.find("label").remove();
      content.find('input').removeClass("js_input_pdf"); 
      content.find("input").removeClass("form-error");  
      content.find("input").addClass("js_input_pdf_clone"); 
      content.find("input").attr('name', 'pdf_additional');
      content.find("input").attr('id', 'pdf_additional');
      content.find("input").val('');
      content.insertAfter($(".js_input_pdf").closest(".content_input"));
      $(this).removeClass("js_add_input_pdf").addClass('js_remove_input_pdf');
      $(this).removeClass("button--add").addClass('button--remove');
      $(this).find('.text').text('Quitar segundo PDF');
      jcf.replaceAll();      
      $(content.find("input")).rules("add", {
        required: true,
        filesize: 5000000,
        extension: "pdf",
        messages: {
          required: 'Campo requerido',
          filesize: 'El pdf ingresado excede 5MB',
          extension: 'El pdf ingresado no es válido'
        }
      });
    });

    // quitar input url de pdf
    $(document).on('click', '.js_remove_input_pdf', function(){
      var input = $("body").find(".js_input_pdf_clone").closest(".content_input").remove();
      $(this).removeClass("js_remove_input_pdf").addClass('js_add_input_pdf');
      $(this).removeClass("button--remove").addClass('button--add');
      $(this).find('.text').text('Añadir otro archivo PDF');
    });

    // agregar nuevo input url de pdf
    $(document).on('click', '.js_add_input_photo', function(){
      var content = $("body").find(".js_input_photo").closest(".content_input").clone();
      content.append(content.find('input'));
      content.find('.jcf-file').remove();
      content.find("label").remove();
      content.find('input').removeClass("js_input_photo"); 
      content.find('input').removeClass("form-error");   
      content.find('input').addClass("js_input_photo_clone"); 
      content.find('input').attr('name', 'photo_additional');
      content.find('input').attr('id', 'photo_additional');
      content.find("input").val('');
      content.insertAfter($(".js_input_photo").closest(".content_input"));
      $(this).removeClass("js_add_input_photo").addClass('js_remove_input_photo');
      $(this).removeClass("button--add").addClass('button--remove');
      $(this).find('.text').text('Quitar segunda foto');
      jcf.replaceAll();
      $(content.find("input")).rules("add", {
        required: true,
        filesize: 5000000,
        extension: "jpg|jpeg|png",
        messages: {
          required: 'Campo requerido',
          filesize: 'La foto ingresada excede 5MB',
          extension: 'La foto ingresada no es válido'
        }
      });
    });

    // quitar input url de pdf
    $(document).on('click', '.js_remove_input_photo', function(){
      var input = $("body").find(".js_input_photo_clone").parent().remove();
      $(this).removeClass("js_remove_input_photo").addClass('js_add_input_photo');
      $(this).removeClass("button--remove").addClass('button--add');
      $(this).find('.text').text('Añadir otra foto');
    });

    // se ejecuta cuando el input select companytype hace alguna seleccion
    $('.js_companytype').on('change', function(){
      setCategories();
    });

    // se ejecuta cuando el input checkbox hace el evento change
    $('.js_companycategory').on('change', function(){
      setQuestions();
      setVerifycheck();
    });

    // verifica si por lo menos hay un checkbox seleccionado.
    function setVerifycheck(){
      $(".js_verifycheck").val("");
      var list_id = "";
      $('.js_companycategory').each(function(){
        if($(this).is(":checked")){
          list_id += $(this).val();
          $(".js_verifycheck").val(list_id);
        }
      })      
      $(".js_verifycheck").focus();
      $(".js_verifycheck").val(list_id);
      $(".js_verifycheck").focusout();
    }

    // muestra oculta y desactiva checkox segun el tipo de empresa
    function setCategories(){
      // obtener todos los los checkbox con el atributo checked
      var list_checkbox = $('.js_companycategory');
      // obtener el option del select
      var select = $('.js_companytype').find(":selected").val();
      // se reinician los checkbox
      list_checkbox.prop('checked', true);
      list_checkbox.trigger('click');
      list_checkbox.closest(".step__row").addClass("step__row--hidden");
      // se recorre todo la lista de checkbox para mostrar solo los 
      // inputs por su tipo de empresa
      list_checkbox.each(function(){
        if($(this).data('companytype')==select){
            $(this).closest(".step__row").removeClass("step__row--hidden");
        }
      });
      $(".js_verifycheck").val("");
    }    
    
    // muestra oculta las preguntas segun la categoria que se seleccione
    function setQuestions(){
      var str_id = "";
      var list_id = [];
      // obtener todos los los checkbox con el atributo checked
      var list_checkbox = $('.js_companycategory:checkbox:checked');  
      // obtener todos los textarea de preguntas ocultas    
      var list_textarea = $('.js_companyquestion');
      // se recorre todo la lista de checkbox para crear una cadena separada por coma
      // la cual se convertira en una lista
      list_checkbox.each(function(index, element){
        if(index===(list_checkbox.length - 1)){
          str_id = str_id + $(this).data('companyquestions');
        }else {
          str_id = str_id + $(this).data('companyquestions')+",";
        }
      });
      // se convierte la cadena en lista
      list_id = str_id.split(",");
      // se quita los repetidos
      list_id = list_id.unique();
      // ocultamos por cada llamada al metodo las preguntas
      list_textarea.closest(".step__row").addClass("step__row--hidden");
      list_textarea.removeClass("js_companyquestion_active");      
      // recorre todo la lista de textarea, se comprueba que el el data exista en list_id
      // dependiendo de eso muestra los inputs
      list_textarea.each(function(){
        if(list_id.indexOf($(this).data('companyquestion').toString())>=0){
          $(this).closest(".step__row").removeClass("step__row--hidden");
          $(this).addClass('js_companyquestion_active');
        }
      });
    }

    // crea modal inicial del formulario
    function setInitialModal(){
      Swal.fire({
        type: "warning",
        title: "Importante",
        html:"<p><b>1.</b> El caso o iniciativa debe ser vigente y tener resultados.</p>"+
             "<p><b>2.</b> Tener toda la información a ingresar en el formulario: archivos, imágenes o videos.</p>"+
             "<p><b>3.</b> Una empresa puede participar máximo en dos categorías. </p>"+
             "<p><b>4.</b> Por favor, verifica la información antes de enviarla.</p>"+ 
             "<p><b>5.</b> Si tiene alguna duda comunicarse al mail <a href='mailto:contacto@premioigualitario.com'>contacto@premioigualitario.com</a></p>",
        showConfirmButton: true,
        confirmButtonText: "Continuar",
        customClass: "swal2-popup--medium",
        showCloseButton: true
      });
    }

    // se ejecuta para que inicialice las categorias del primer option 
    // que viene por default
    setCategories();
    // se ejectuta para mostrar el modal inicial
    setInitialModal();

};

FormCase.prototype.inputrules = function(){

  // valida los numero enteros, alfanumerico, etc
  function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
      });
    });
  }

  $("#representative").alpha({
    disallow: '&,;._-!|"·#@$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…'
  });

  $("#email, #name, #initiative").alphanum({
    disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
    allow: '@._-'
  });
  
  setInputFilter(document.getElementById("ruc"), function(value) {
    return /^\d*$/.test(value); 
  });

  setInputFilter(document.getElementById("phone"), function(value) {
    return /^\d*$/.test(value); 
  });

};

FormCase.prototype.onPageReady = function(){
    $('#loader').fadeOut('slow', function(e){
        $(this).remove();
    });
};
module.exports = FormCase;