var Login = function(){
    this.form;
    this.initialize();
    this.onPageReady();
};

Login.prototype.initialize = function(){
  var self = this;
  self.form = $("#form_login");
  self.validate();
  self.handlers();
};

Login.prototype.handlers = function(){
    var self  = this;

    $("#form_login").on('submit', function (event) {
        var isValid = self.form.valid();
        if(!isValid) {
            event.preventDefault();
        }
    });
};

Login.prototype.validate = function(){
  var self  = this;
  self.form.validate({
    debug: false,
    errorClass: 'form-error',
    messages: {
      username: {
        required: "",
        minlength: "",
        maxlength: "",
      },
      password: {
        required: "",
        minlength: "",
        maxlength: "",
      }
    },
    rules: {
      username: {
        required: true,
        minlength: 2,
        maxlength: 250,
      },
      password: {
        required: true,
        minlength: 2,
        maxlength: 250,
      }
    },
    errorPlacement: function (error, element) {
        error.insertAfter(element);
    },
    highlight: function(element, errorClass, validClass) {
      element = $(element);
      element.addClass('form-error');
    },
    unhighlight: function (element, errorClass, validClass) {
      element = $(element);
      element.removeClass('form-error');
    }
  });
};

Login.prototype.onPageReady = function(){
    $('#loader').fadeOut('slow', function(e){
        $(this).remove();
    });
};
module.exports = Login;