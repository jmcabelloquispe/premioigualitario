var ResetPassword = function(){
  this.form;
  this.initialize();
  this.onPageReady();
};

Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});

ResetPassword.prototype.initialize = function(){
  var self = this;
  self.form = $("#form_resetpassword");
  self.validate();
  self.handlers();
  self.inputrules();
};

ResetPassword.prototype.validate = function(){
  var self  = this;
  self.form.validate({
    debug: false,
    errorClass: 'form-error',
    messages: {
      email: {
        required: "Campo requerido",
        minlength: "El correo ingresado no es válido",
        maxlength: "El correo ingresado no es válido",
        validEmail: "El correo ingresado no es válido",
      }
    },
    rules: {
      email: {
        required: true,
        minlength: 6,
        maxlength: 120,
        validEmail:true,
      },
    },
    errorPlacement: function (error, element) {
      if(element.hasClass('js_terms')){
        error.insertAfter(element.parent().parent());
      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
        error.insertAfter(element.parent());
      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
        error.insertAfter(element.parent());
      }
      else {
        error.insertAfter(element);
      }
    },
    highlight: function(element, errorClass, validClass) {
      element = $(element);
      if(element.hasClass('js_terms')){
        element.parent().addClass('form-error');
        // element.parent().next().addClass('label-error');
      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
        element.parent().addClass('form-error');
      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
        element.parent().addClass('form-error');
      }
      else {
        element.addClass('form-error');
      }
    },
    unhighlight: function (element, errorClass, validClass) {
      element = $(element);
      if(element.hasClass('js_terms')){
        element.parent().removeClass('form-error');
        // element.parent().next().removeClass('label-error');
      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
        element.parent().removeClass('form-error');
      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
        element.parent().removeClass('form-error');
      }
      else {
        element.removeClass('form-error');
      }
    }
  });
}

ResetPassword.prototype.handlers = function(){
    var self  = this;

    $("#form_resetpassword").on('submit', function (event) {
        var isValid = self.form.valid();
        if(!isValid) {
            event.preventDefault();
        }
    });

    setTimeout(function () {
      $(".resetpassword__row--error").css('display', 'none')
    }, 3000);
};

ResetPassword.prototype.inputrules = function(){

  // valida los numero enteros, alfanumerico, etc
  function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
      });
    });
  }


  $("#email").alphanum({
    disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
    allow: '@._-'
  });

};

ResetPassword.prototype.onPageReady = function(){
    $('#loader').fadeOut('slow', function(e){
        $(this).remove();
    });
};
module.exports = ResetPassword;