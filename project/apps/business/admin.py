#!encoding:utf-8
from datetime import date

from django.contrib import admin, messages
from django.db.models import Sum

from business.models import CompanyType, CompanyCategory, CompanyQuestion, Company, CompanyDetail, \
    CompanyDetailEvaluation, CompanyCategoryCriteria
from django.utils.safestring import mark_safe
from common.utils import ExcelResponse
from django.urls import path
from django.shortcuts import redirect
from django.urls import reverse


class CompanyTypeAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_at', )

admin.site.register(CompanyType, CompanyTypeAdmin)

class CompanyCategoryAdmin(admin.ModelAdmin):

    list_display = ('name', 'companytype', 'minimum_score', 'created_at', )
    list_filter = ('companytype',)

admin.site.register(CompanyCategory, CompanyCategoryAdmin)

class CompanyQuestionAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'companycategory', 'created_at',)
    list_filter = ('companycategory',)

admin.site.register(CompanyQuestion, CompanyQuestionAdmin)

class CompanyCategoryCriteriaAdmin(admin.ModelAdmin):

    list_display = ('id', 'companycategory', 'title', 'name_input', )

admin.site.register(CompanyCategoryCriteria, CompanyCategoryCriteriaAdmin)

class CompanyDetailInline(admin.TabularInline):

    model = CompanyDetail
    extra = 0
    min_num = 1
    max_num = 20


class CompanyDetailEvaluationInline(admin.TabularInline):

    model = CompanyDetailEvaluation
    extra = 0
    min_num = 0
    max_num = 7


class CompanyAdmin(admin.ModelAdmin):

    list_display = ('id', 'state', 'data_state', 'score_preliminary', 'score_final', 'ruc', 'name', 'companytype', 'representative',
                    'email', 'phone', 'get_categories',
                    'get_created_at')
    list_filter = ('companytype', 'companycategories', 'state', 'data_state')
    search_fields = ('name', 'representative', 'email')
    actions = ['export']
    inlines = (CompanyDetailInline, CompanyDetailEvaluationInline)
    ordering = ['-id']
    list_per_page = 10
    fieldsets = (
        ('Datos de la Empresa' , {
            'fields': ('name', 'ruc', 'companytype', 'representative', 'email', 'phone')
        }),
        ('Datos del Caso', {
            'fields': ('companycategories', 'videourl', 'videourl_additional', 'pdf', 'pdf_additional', 'photo',
                       'photo_additional', 'initiative'),
        }),
    )

    def get_categories(self, obj):
        return mark_safe("".join(["<p style='margin: 0;'>" + i.name +"</p>" for i in obj.companycategories.all()]))
    get_categories.short_description = u'Categoría'

    def get_created_at(self, obj):
        return obj.created_at.strftime("%d/%m/%Y")
    get_created_at.admin_order_field = 'created_at'
    get_created_at.short_description = u'F. Creación'

    def changelist_view(self, request, extra_context=None):
        extra_context = {'title': 'Empresas'}
        return super(CompanyAdmin, self).changelist_view(request, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = {'title': 'Detalle de Empresa'}
        return self.changeform_view(request, object_id, form_url, extra_context)

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        company = Company.objects.filter(pk=object_id)
        category = ''
        if company.get().companycategories.exists():
            category = True if Company.objects.filter(state=Company.WINNER,
                                                      companycategories=company.first().companycategories.first().pk) \
                else False
        else:
            category = False
        extra_context['company'] = company
        extra_context['state_postulant'] = Company.POSTULANT
        extra_context['state_suitable'] = Company.SUITABLE
        extra_context['state_rejected'] = Company.REJECTED
        extra_context['state_winner'] = Company.WINNER
        extra_context['state_qualified_finalist'] = Company.QUALIFIED_FINALIST
        extra_context['no_winner'] = category
        extra_context['data_state_incomplete'] = Company.INCOMPLETE
        extra_context['data_state_complete'] = Company.COMPLETE
        extra_context['eval_preliminary'] = \
            CompanyDetailEvaluation.objects.filter(company=object_id,
                                                   type=CompanyDetailEvaluation.PRELIMINARY)
        extra_context['eval_preliminary_score'] = \
            CompanyDetailEvaluation.objects.filter(company=object_id,
                                                   type=CompanyDetailEvaluation.PRELIMINARY).aggregate(Sum('score'))
        extra_context['eval_final'] = \
            CompanyDetailEvaluation.objects.filter(company=object_id,
                                                   type=CompanyDetailEvaluation.FINAL)
        extra_context['eval_final_score'] = \
            CompanyDetailEvaluation.objects.filter(company=object_id,
                                                   type=CompanyDetailEvaluation.FINAL).aggregate(Sum('score'))
        return super(CompanyAdmin, self).changeform_view(request, object_id, extra_context=extra_context)

    def get_urls(self):
        urls = super(CompanyAdmin, self).get_urls()
        my_urls = [
            path('<int:pk>/update/<slug:state>/', self.admin_site.admin_view(self.company_evaluation),
                 name='company_evaluation'),
        ]
        return my_urls + urls

    def company_evaluation(self, request, **kwargs):
        company_id = kwargs.get('pk')
        company_state = kwargs.get('state')
        try:
            company = Company.objects.get(pk=company_id)

            if company_state == Company.SUITABLE:
                company.state = company_state
                messages.info(request, u"{0} {1}".format('Se aprobó la empresa', company.name))
            elif company_state == Company.REJECTED:
                company.state = company_state
                messages.info(request, u"{0} {1}".format('Se rechazó la empresa', company.name))
            elif company_state == Company.WINNER:
                company_winner = Company.objects.filter(state=Company.WINNER,
                                                        companycategories=company.companycategories.first().pk)
                if company_winner:
                    messages.error(request, u'{0} {1}'.format('Ya existe un ganador en la categoría',
                                                              company.companycategories.first()))
                else:
                    company.state = company_state
                    messages.info(request, u'La empresa {0} ganó el concurso en la categoría {1}'.
                                  format(company.name,company.companycategories.first()))
            company.save()

        except Exception as exception:
            messages.error(request, exception)
        url = reverse('admin:business_company_changelist')
        return redirect(url)

    def export(self, request, queryset):
        header_list = [(u'Id', u'Estado', u'Estado Formulario', u'Puntaje Preliminar', u'Puntaje Final', u'Ruc',
                        u'Empresa', u'Tipo de Empresa', u'Categoría', u'Representante', u'Correo electrónico',
                        u'Teléfono', u'Fecha de Creación')]
        data_list = []
        list_product = queryset.values('id', 'state', 'data_state', 'score_preliminary', 'score_final',
                                       'ruc', 'name', 'companytype__name', 'representative', 'email',
                                       'phone', 'created_at', 'companycategories__name')
        today = date.today()
        today = today.strftime("%d%m%Y")
        for attribute in list_product:
            row = (
                attribute.get('id'),
                dict(Company.STATE_CHOICES)[attribute.get('state')],
                dict(Company.DATA_STATE_CHOICES)[attribute.get('data_state')],
                attribute.get('score_preliminary'),
                attribute.get('score_final'),
                attribute.get('ruc'),
                attribute.get('name'),
                attribute.get('companytype__name'),
                attribute.get('companycategories__name'),
                attribute.get('representative'),
                attribute.get('email'),
                attribute.get('phone'),
                attribute.get('created_at').strftime("%d/%m/%Y"),
            )
            data_list.append(row)
        return ExcelResponse(data=header_list + data_list, output_name='Lista_de_Empresas_'+ today)
    export.short_description = u"Descargar Empresa/s seleccionada/s"

    # media
    class Media:
        css = {
            "all": ('business/css/company.css',)
        }

admin.site.register(Company, CompanyAdmin)