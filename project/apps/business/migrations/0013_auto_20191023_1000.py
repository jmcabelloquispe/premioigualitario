# Generated by Django 2.1.7 on 2019-10-23 15:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0012_auto_20191016_0733'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='consists',
        ),
        migrations.RemoveField(
            model_name='company',
            name='impact',
        ),
        migrations.RemoveField(
            model_name='company',
            name='objective',
        ),
    ]
