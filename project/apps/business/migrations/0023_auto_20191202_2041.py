# Generated by Django 2.1.7 on 2019-12-03 01:41

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0022_company_data_state'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='initiative',
            field=models.TextField(blank=True, max_length=255, validators=[django.core.validators.RegexValidator('^[ -~áéíóúÁÉÍÓÚñÑüÜ¡¿]+$')], verbose_name='Nombre de la iniciativa'),
        ),
    ]
