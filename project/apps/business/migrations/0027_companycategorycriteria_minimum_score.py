# Generated by Django 2.1.7 on 2019-12-15 09:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0026_auto_20191215_0419'),
    ]

    operations = [
        migrations.AddField(
            model_name='companycategorycriteria',
            name='minimum_score',
            field=models.IntegerField(default=0, verbose_name='Puntaje Minimo'),
        ),
    ]
