# Generated by Django 2.1.7 on 2019-12-15 09:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0027_companycategorycriteria_minimum_score'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='companycategorycriteria',
            name='minimum_score',
        ),
        migrations.AddField(
            model_name='companycategory',
            name='minimum_score',
            field=models.IntegerField(default=0, verbose_name='Puntaje Minimo'),
        ),
    ]
