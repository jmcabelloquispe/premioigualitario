# Generated by Django 2.1.7 on 2020-01-02 17:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0036_auto_20191225_0852'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='initiative',
            field=models.TextField(blank=True, max_length=255, verbose_name='Nombre de la iniciativa'),
        ),
    ]
