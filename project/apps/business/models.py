#!encoding:utf-8
from django.db import models
from django.contrib.auth.models import User
from common.validators import phone_validator, name_validator, numeric_validator, alphanumeric_validator
from django.db.models import signals
from django.db.models import Sum


class CompanyType(models.Model):

    name = models.CharField(
        verbose_name=u'Nombre',
        max_length=200,
        blank=False,
    )

    created_at = models.DateTimeField(
        verbose_name=u'Fecha de Creación',
        auto_now_add=True
    )

    def __str__(self):
        return u'{0}'.format(self.name)

    class Meta:
        verbose_name = u'Tipo de Empresa'
        verbose_name_plural = u'Tipo de Empresa'
        ordering = ('id',)


class CompanyCategory(models.Model):

    name = models.CharField(
        verbose_name=u'Nombre',
        max_length=200,
        blank=False,
    )

    companytype = models.ForeignKey(
        CompanyType,
        verbose_name=u'Tipo de Empresa',
        on_delete=models.PROTECT
    )

    created_at = models.DateTimeField(
        verbose_name=u'Fecha de Creación',
        auto_now_add=True
    )

    companyquestions = models.CharField(
        verbose_name=u'Lista de Preguntas',
        max_length = 200,
        blank = False,
    )

    minimum_score = models.DecimalField(
        verbose_name=u'Puntaje Minimo',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    def __str__(self):
        return u'{0}'.format(self.name)

    class Meta:
        verbose_name = u'Categoría de Empresa'
        verbose_name_plural = u'Categoría de Empresa'
        ordering = ('id',)


class CompanyQuestion(models.Model):

    name = models.CharField(
        verbose_name=u'Pregunta',
        max_length=200,
        blank=False,
    )

    companycategory = models.ForeignKey(
        CompanyCategory,
        verbose_name=u'Categoría de Empresa',
        on_delete=models.PROTECT
    )

    created_at = models.DateTimeField(
        verbose_name=u'Fecha de Creación',
        auto_now_add=True
    )

    def __str__(self):
        return u'{0}'.format(self.name)

    class Meta:
        verbose_name = u'Preguntas por Categoría'
        verbose_name_plural = u'Preguntas por Categoría'
        ordering = ('id',)


class CompanyCategoryCriteria(models.Model):

    companycategory = models.ForeignKey(
        CompanyCategory,
        verbose_name=u'Categoría de Empresa',
        on_delete=models.PROTECT
    )

    title = models.CharField(
        verbose_name=u'Título',
        max_length=200,
        blank=False,
    )

    name_input = models.CharField(
        verbose_name=u'Nombre del Campo',
        max_length=200,
        blank=False,
    )

    def __str__(self):
        return u'{0}'.format(self.title)

    class Meta:
        verbose_name = u'Criterio de Evaluación por Categoría'
        verbose_name_plural = u'Criterio de Evaluación por Categoría'
        ordering = ('id',)


class Company(models.Model):

    POSTULANT = 'POSTULANT'
    SUITABLE = 'SUITABLE'
    QUALIFIED = 'QUALIFIED'
    FINALIST = 'FINALIST'
    QUALIFIED_FINALIST = 'QUALIFIED_FINALIST'
    WINNER = 'WINNER'
    REJECTED = 'REJECTED'

    STATE_CHOICES = (
        (POSTULANT, u'Postulante'),
        (SUITABLE, u'Por calificar'),
        (QUALIFIED, u'Calificado'),
        (FINALIST, u'Finalista'),
        (QUALIFIED_FINALIST, u'Finalista calificado'),
        (WINNER, u'Ganador'),
        (REJECTED, u'Rechazado'),
    )

    INCOMPLETE = 'INCOMPLETE'
    COMPLETE = 'COMPLETE'

    DATA_STATE_CHOICES = (
        (INCOMPLETE, u'Incompleto'),
        (COMPLETE, u'Completo'),
    )

    name = models.CharField(
        verbose_name=u'Empresa',
        validators=[alphanumeric_validator],
        max_length=120,
        blank=False
    )

    ruc = models.CharField(
        verbose_name=u'Ruc',
        validators=[numeric_validator],
        max_length=11,
        blank=False
    )

    companytype = models.ForeignKey(
        CompanyType,
        verbose_name=u'Tipo de Empresa',
        on_delete=models.PROTECT
    )

    representative = models.CharField(
        verbose_name=u'Representante',
        validators=[name_validator],
        max_length=120,
        blank=False
    )

    email = models.EmailField(
        verbose_name=u'Correo electrónico',
        max_length=120,
        blank=False,
    )

    phone = models.CharField(
        verbose_name=u'Teléfono',
        validators=[phone_validator],
        max_length=9,
        blank=False,
    )

    terms = models.BooleanField(
        verbose_name=u'Términos y Condiciones y Políticas de Privacidad',
        default=False
    )

    companycategories = models.ManyToManyField(
        CompanyCategory,
        verbose_name=u'Categoría'
    )

    videourl = models.URLField(
        verbose_name=u'URL con el video del caso',
        max_length=255,
        blank=True,
    )

    videourl_additional = models.URLField(
        verbose_name=u'URL con el video del caso',
        max_length=255,
        blank=True,
    )

    pdf = models.FileField(
        verbose_name=u'PDF con información del caso',
        max_length=255,
        blank=True,
        upload_to='pdf/'
    )

    pdf_additional = models.FileField(
        verbose_name=u'PDF con información del caso',
        max_length=255,
        blank=True,
        upload_to='pdf/'
    )

    photo = models.FileField(
        verbose_name=u'Evidencia en foto de la iniciativa',
        max_length=255,
        blank=True,
        upload_to='foto/'
    )

    photo_additional = models.FileField(
        verbose_name=u'Evidencia en foto de la iniciativa',
        max_length=255,
        blank=True,
        upload_to='foto/'
    )

    initiative = models.TextField(
        verbose_name=u'Nombre de la iniciativa',
        max_length=255,
        blank=True,
    )

    state = models.CharField(
        verbose_name=u'Estado',
        max_length=20,
        choices=STATE_CHOICES,
        default=POSTULANT
    )

    created_at = models.DateTimeField(
        verbose_name=u'Fecha de Creación',
        auto_now_add=True
    )

    score_preliminary = models.DecimalField(
        verbose_name=u'Puntaje',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    score_final = models.DecimalField(
        verbose_name=u'Puntaje Final',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    data_state = models.CharField(
        verbose_name=u'Estado Formulario',
        max_length=20,
        choices=DATA_STATE_CHOICES,
        default=INCOMPLETE
    )

    def __str__(self):
        return u'{0}'.format(self.name)

    class Meta:
        verbose_name = u'Empresa'
        verbose_name_plural = u'Empresas'
        ordering = ('id',)
        permissions = (
            ('company_preliminary_jury_get', u'Can access preliminary jury dashboard'),
            ('company_final_jury_get', u'Can access final jury dashboard'),
        )


class CompanyDetail(models.Model):

    company = models.ForeignKey(
        Company,
        verbose_name=u'Compañia',
        on_delete=models.CASCADE,
        related_name='questions'
    )

    question = models.ForeignKey(
        CompanyQuestion,
        verbose_name=u'Pregunta',
        on_delete=models.PROTECT
    )

    response = models.TextField(
        verbose_name=u'Respuesta',
        max_length=700,
        blank=True,
    )

    def __str__(self):
        return u''.format("#", self.id)

    class Meta:
        verbose_name = u'Pregunta según la Categoría'
        verbose_name_plural = u'Preguntas según la Categoría'
        ordering = ('id',)


class CompanyDetailEvaluation(models.Model):

    MAX_EVALUATION_PRELIMINARY = 3
    MAX_EVALUATION_FINAL = 5
    PERMISSION_PRELIMINARY = 'business.company_preliminary_jury_get'
    PERMISSION_FINAL = 'business.company_final_jury_get'

    PRELIMINARY = 'PRELIMINARY'
    FINAL = 'FINAL'

    TYPE_CHOICES = (
        (PRELIMINARY, u'Preliminar'),
        (FINAL, u'Final'),
    )

    company = models.ForeignKey(
        Company,
        verbose_name=u'Compañia',
        on_delete=models.CASCADE
    )

    jury = models.ForeignKey(
        User,
        verbose_name=u'Nombre',
        on_delete=models.CASCADE
    )

    comment = models.CharField(
        verbose_name=u'Comentario',
        max_length=250,
        blank=False
    )

    score = models.DecimalField(
        verbose_name=u'Puntaje',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    type = models.CharField(
        verbose_name=u'Tipo',
        max_length=20,
        choices=TYPE_CHOICES,
        default=PRELIMINARY
    )

    created_at = models.DateTimeField(
        verbose_name=u'Fecha de Creación',
        auto_now_add=True
    )

    impact = models.DecimalField(
        verbose_name=u'Impacto',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    creativity = models.DecimalField(
        verbose_name=u'Creatividad',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    social_change = models.DecimalField(
        verbose_name=u'Cambio Social',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    sustainability = models.DecimalField(
        verbose_name=u'Sostenibilidad',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    originality = models.DecimalField(
        verbose_name=u'Originalidad',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    scope = models.DecimalField(
        verbose_name=u'Alcance',
        default=0,
        max_digits=10,
        decimal_places=2
    )

    def save(self, *args, **kwargs):
        self.score = self.impact + self.creativity + self.social_change + self.sustainability + self.originality + \
                     self.scope
        super(CompanyDetailEvaluation, self).save(*args, **kwargs)

    def __str__(self):
        return u''.format("#", self.id)

    class Meta:
        verbose_name = u'Calificación'
        verbose_name_plural = u'Calificaciones'
        ordering = ('id',)


def send_company_detail_evaluation(sender, instance, **kwargs):

    evaluation_list = CompanyDetailEvaluation.objects.filter(company=instance.company, type=instance.type)
    total_score = evaluation_list.aggregate(Sum('score'))
    company = Company.objects.get(id=instance.company.id)
    if instance.type == CompanyDetailEvaluation.PRELIMINARY:
        company.score_preliminary = 0 if total_score['score__sum'] is None else total_score['score__sum']
        if evaluation_list.count() >= CompanyDetailEvaluation.MAX_EVALUATION_PRELIMINARY:
            if total_score['score__sum'] >= company.companycategories.first().minimum_score:
                company.state = Company.FINALIST
            else:
                company.state = Company.QUALIFIED
        else:
            company.state = Company.SUITABLE
    else:
        company.score_final = 0 if total_score['score__sum'] is None else total_score['score__sum']
        if evaluation_list.count() >= CompanyDetailEvaluation.MAX_EVALUATION_FINAL:
            company.state = Company.QUALIFIED_FINALIST
        else:
            company.state = Company.FINALIST
    company.save()


signals.post_save.connect(send_company_detail_evaluation, sender=CompanyDetailEvaluation, dispatch_uid=None)
signals.post_delete.connect(send_company_detail_evaluation, sender=CompanyDetailEvaluation, dispatch_uid=None)
