#!encoding:utf-8
import os
from rest_framework import serializers
from business.models import Company, CompanyDetail, CompanyType, CompanyDetailEvaluation
from django.conf import settings
from common.utils import GeneratePDF
from common.mail.main import send_email


class CompanyDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyDetail
        fields = ('question', 'response')


class CompanyDetailEvaluationPreliminarySerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyDetailEvaluation
        fields = ('company', 'jury', 'comment', 'impact', 'creativity', 'social_change', 'sustainability',
                  'originality', 'scope')


class CompanyDetailEvaluationFinalSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyDetailEvaluation
        fields = ('company', 'jury', 'type', 'comment', 'impact', 'creativity', 'social_change', 'sustainability',
                  'originality', 'scope')


class CompanyStepOneSerializer(serializers.ModelSerializer):
    queryset = None

    class Meta:
        model = Company
        fields = ('name', 'ruc', 'companytype', 'representative', 'email', 'phone', 'terms')

    def create(self, validated_data):
        ruc = validated_data['ruc']
        company, created = Company.objects.update_or_create(
            ruc=ruc, data_state=Company.INCOMPLETE, defaults={**validated_data})
        return company


class CompanyStepTwoSerializer(serializers.ModelSerializer):
    queryset = None
    questions = CompanyDetailSerializer(many=True)

    class Meta:
        model = Company
        fields = ('name', 'ruc', 'companytype', 'representative', 'email', 'phone', 'terms', 'companycategories',
                  'videourl', 'videourl_additional', 'pdf', 'pdf_additional', 'photo', 'photo_additional', 'initiative',
                  'questions')

    def create(self, validated_data):
        ruc = validated_data['ruc']
        questions = validated_data.pop('questions')
        categories = validated_data.pop('companycategories')
        company = ""
        validated_data['data_state'] = Company.COMPLETE
        try:
            Company.objects.filter(ruc=ruc, data_state=Company.INCOMPLETE).delete()
            # recorre las categorias
            for category in categories:
                company = Company.objects.create(**validated_data)
                company.companycategories.set([category])
                # clasifica por categoria las preguntas
                for question in questions:
                    if str(question['question'].id) in category.companyquestions.split(','):
                        CompanyDetail.objects.create(company=company, **question)
                #envio de correo
                question_list = CompanyDetail.objects.filter(company=company.pk)
                data = {
                    'BASE_URL': settings.BASE_URL,
                    'company': company,
                    'questions': question_list,
                    'pdf': os.path.basename(company.pdf.name),
                    'pdf_additional': os.path.basename(company.pdf_additional.name),
                    'photo': os.path.basename(company.photo.name),
                    'photo_additional': os.path.basename(company.photo_additional.name)
                }
                pdf = GeneratePDF.export('post_pdf.html', data)
                attachment = ("registro_de_iniciativa.pdf", pdf)
                send_email(settings.FROM_EMAIL, company.email, settings.SUBJECT, 'mailing.html', data=data,
                           attachment=attachment)
        except Exception as e:
            print(e)
        return company


class CompanyTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyType
        fields = '__all__'
