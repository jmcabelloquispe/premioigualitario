# coding:utf-8
from django.urls import path
from business import views

app_name = 'business'

urlpatterns = [
    path('company/', views.CompanyStepOneList.as_view(), name='company'),
    path('company/step/two/', views.CompanyStepTwoList.as_view(), name='company_step_two'),
    path('companytype/', views.CompanyTypeList.as_view(), name='companytype'),
    path('company/preliminary/evaluation/', views.CompanyDetailEvaluationPreliminaryList.as_view(),
         name='company_preliminary_evaluation'),
    path('company/final/evaluation/', views.CompanyDetailEvaluationFinalList.as_view(),
         name='company_final_evaluation'),
]
