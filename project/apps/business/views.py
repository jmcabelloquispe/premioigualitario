from django.http import HttpResponse
from easy_pdf.rendering import html_to_pdf
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics
from rest_framework.response import Response
from business.models import CompanyType, Company, CompanyCategory, CompanyDetailEvaluation
from business.serializers import CompanyStepOneSerializer, CompanyStepTwoSerializer, CompanyTypeSerializer, \
    CompanyDetailEvaluationPreliminarySerializer, CompanyDetailEvaluationFinalSerializer


class CompanyStepOneList(generics.ListCreateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyStepOneSerializer

    def get_queryset(self):
        queryset = None
        flag = self.request.query_params.get('flag')
        categories = self.request.query_params.get('categories')
        ruc = self.request.query_params.get('ruc')
        email = self.request.query_params.get('email')
        phone = self.request.query_params.get('phone')
        if email:
            queryset = Company.objects.filter(email=email)
        elif phone:
            queryset = Company.objects.filter(phone=phone)
        elif ruc and categories and flag:
            list_category = [int(x) for x in str(categories)]
            exists = False
            for i in list_category:
                if CompanyCategory.objects.filter(company__ruc=ruc, pk=i):
                    exists = True
                    break
            if exists:
                queryset = Company.objects.filter(ruc=ruc)
            else:
                queryset = Company.objects.filter(ruc="")
        elif ruc and categories:
            ruc_category_count = CompanyCategory.objects.filter(company__ruc=ruc).values('pk').count()
            list_category = [int(x) for x in str(categories)]
            category_count = CompanyCategory.objects.all().count()
            if ruc_category_count + len(list_category) > category_count:
                queryset = Company.objects.filter(ruc=ruc)
            else:
                queryset = Company.objects.filter(ruc="")
        elif ruc:
            ruc_category_count = CompanyCategory.objects.filter(company__ruc=ruc).values('pk').count()
            category_count = CompanyCategory.objects.all().count()
            if ruc_category_count == category_count:
                queryset = Company.objects.filter(ruc=ruc)
            else:
                queryset = Company.objects.filter(ruc="")
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.get_queryset().exists()
            serializer = queryset
            return Response({"exists": serializer})
        except Exception as e:
            print(e)
            return HttpResponse(status=404)


class CompanyStepTwoList(generics.ListCreateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyStepTwoSerializer

    def get_queryset(self):
        queryset = None
        flag = self.request.query_params.get('flag')
        categories = self.request.query_params.get('categories')
        ruc = self.request.query_params.get('ruc')
        email = self.request.query_params.get('email')
        phone = self.request.query_params.get('phone')
        if email:
            queryset = Company.objects.filter(email=email)
        elif phone:
            queryset = Company.objects.filter(phone=phone)
        elif ruc and categories and flag:
            list_category = [int(x) for x in str(categories)]
            exists = False
            for i in list_category:
                if CompanyCategory.objects.filter(company__ruc=ruc, pk=i):
                    exists = True
                    break
            if exists:
                queryset = Company.objects.filter(ruc=ruc)
            else:
                queryset = Company.objects.filter(ruc="")
        elif ruc and categories:
            ruc_category_count = CompanyCategory.objects.filter(company__ruc=ruc).values('pk').count()
            list_category = [int(x) for x in str(categories)]
            category_count = CompanyCategory.objects.all().count()
            if ruc_category_count + len(list_category) > category_count:
                queryset = Company.objects.filter(ruc=ruc)
            else:
                queryset = Company.objects.filter(ruc="")
        elif ruc:
            ruc_category_count = CompanyCategory.objects.filter(company__ruc=ruc).values('pk').count()
            category_count = CompanyCategory.objects.all().count()
            if ruc_category_count == category_count:
                queryset = Company.objects.filter(ruc=ruc)
            else:
                queryset = Company.objects.filter(ruc="")
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.get_queryset().exists()
            serializer = queryset
            return Response({"exists": serializer})
        except Exception as e:
            print(e)
            return HttpResponse(status=404)


class CompanyTypeList(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = CompanyType.objects.all()
    serializer_class = CompanyTypeSerializer


class CompanyDetailEvaluationPreliminaryList(generics.ListCreateAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = CompanyDetailEvaluation.objects.all()
    serializer_class = CompanyDetailEvaluationPreliminarySerializer


class CompanyDetailEvaluationFinalList(generics.ListCreateAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = CompanyDetailEvaluation.objects.all()
    serializer_class = CompanyDetailEvaluationFinalSerializer
