# coding:utf-8


from django.conf import settings


def common(request):

    settings_to_include = [
        'BASE_URL',
        'TITLE',
        'DESCRIPTION',
        'KEYWORDS',
        'AUTHOR',
        'SHARE_TITLE',
        'SHARE_DESCRIPTION',
        'AUTHOR',
        'KEYWORDS',
        'FACEBOOK_ID',
    ]

    return {
        name: getattr(settings, name)
        for name in settings_to_include if hasattr(settings, name)
    }
