# -*- coding: utf-8 -*-
from django.conf import settings
from .mailgun import MailGunHelper
from django.template.loader import get_template


def send_email(from_email, to_email, subject, template, data, attachment):

    if not settings.SEND_EMAILS:
        print('Activate SEND_EMAILS var in settings to send transactional emails')
        return

    mghelper = MailGunHelper()
    mghelper.send_simple_message(
        from_email=from_email,
        to_email=to_email,
        subject=subject,
        body=get_template(template).render(data),
        attachment=attachment,
    )


def send_simple_email(from_email, to_email, subject, template, data):

    if not settings.SEND_EMAILS:
        print('Activate SEND_EMAILS var in settings to send transactional emails')
        return

    mghelper = MailGunHelper()
    mghelper.send_message(
        from_email=from_email,
        to_email=to_email,
        subject=subject,
        body=get_template(template).render(data),
    )