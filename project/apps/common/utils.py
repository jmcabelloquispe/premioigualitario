# coding:utf-8
"""
Utilidades Varias
=================
"""
import logging
import os
import string

import openpyxl
import random

import time
from django.conf import settings
from django.http import HttpResponse
from django.template import loader
from easy_pdf.exceptions import UnsupportedMediaPathException
from easy_pdf.rendering import html_to_pdf
logger = logging.getLogger(__name__)


class Numbers:
    """
    Clase de utilidades para trabajar con datos de tipo numérico.
    """

    @staticmethod
    def is_number(s):
        """
        Comprueba que un dato es del tipo numérico o contiene números.

        :param s: Dato que será comprobado.
        :return: ``True`` si ``s`` es numérico, ``False`` de otra forma.

        Uso:

        .. code:: python

            Numbers.is_number('123')  # True
            Numbers.is_number('uno')  # False
            Numbers.is_number(123)    # True
        """

        try:
            float(s)
            return True
        except ValueError:
            pass

        try:
            import unicodedata
            unicodedata.numeric(s)
            return True
        except (TypeError, ValueError):
            pass

        return False


class RandomUtil:
    """
    Clase de utilidades para trabajar con números aleatorios.
    """

    @staticmethod
    def get_random_probability(option_probability_dict=None):
        """
        TODO: Llenar

        :param option_probability_dict: To be implemented
        :return: None
        """

        random_number = random.random()
        probabilities_sum = 0.0
        prize_id = 0

        for key in option_probability_dict:

            probability = option_probability_dict[key] / 100.0

            probability_max = probability + probabilities_sum

            if random_number <= probability_max:
                prize_id = key
                break

            probabilities_sum += probability

        return prize_id


class ExcelResponse(HttpResponse):

    def __init__(self, data, output_name='excel_data', headers=None):

        # Make sure we've got the right type of data to work with
        valid_data = False
        if hasattr(data, '__getitem__') and not type(data) == dict:
            if isinstance(data[0], dict):
                if headers is None:
                    headers = data[0].keys()
                data = [[row[col] for col in headers] for row in data]
                data.insert(0, headers)
            if hasattr(data[0], '__getitem__'):
                valid_data = True
        elif type(data) == dict:
            for value in data.values():
                if isinstance(value[0], dict):
                    if headers is None:
                        headers = value[0].keys()
                    value = [[row[col] for col in headers] for row in value]
                    value.insert(0, headers)
                if hasattr(value[0], '__getitem__'):
                    valid_data = True

        assert valid_data is True, \
            "ExcelResponse requires a sequence of sequences or a dictionary with sequences as values"

        import io
        output = io.BytesIO()

        book = openpyxl.Workbook()

        if type(data) != dict:
            sheet = book.active

            for rowx, row in enumerate(data):
                for colx, value in enumerate(row):
                    cell = sheet.cell(row=rowx+1, column=colx+1)
                    cell.value = value
        else:
            for key, val in data.items():
                del book.worksheets[0]
                sheet = book.create_sheet(title=key)

                for rowx, row in enumerate(val):
                    for colx, value in enumerate(row):
                        cell = sheet.cell(row=rowx+1, column=colx+1)
                        cell.value = value

        book.save(output)

        mimetype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        file_ext = 'xlsx'

        output.seek(0)
        super(ExcelResponse, self).__init__(content=output.getvalue(), content_type=mimetype)
        self['Content-Disposition'] = 'attachment;filename="%s.%s"' % (output_name.replace('"', '\"'), file_ext)


class ExcelFile:

    file_name = None

    def __init__(self, data, headers=None):

        # Make sure we've got the right type of data to work with
        valid_data = False
        if hasattr(data, '__getitem__') and not type(data) == dict:
            if isinstance(data[0], dict):
                if headers is None:
                    headers = data[0].keys()
                data = [[row[col] for col in headers] for row in data]
                data.insert(0, headers)
            if hasattr(data[0], '__getitem__'):
                valid_data = True
        elif type(data) == dict:
            for value in data.values():
                if isinstance(value[0], dict):
                    if headers is None:
                        headers = value[0].keys()
                    value = [[row[col] for col in headers] for row in value]
                    value.insert(0, headers)
                if hasattr(value[0], '__getitem__'):
                    valid_data = True

        assert valid_data is True, \
            "ExcelResponse requires a sequence of sequences or a dictionary with sequences as values"

        book = openpyxl.Workbook()

        if type(data) != dict:
            sheet = book.active

            for rowx, row in enumerate(data):
                for colx, value in enumerate(row):
                    cell = sheet.cell(row=rowx + 1, column=colx + 1)
                    cell.value = value
        else:
            for key, val in data.items():
                del book.worksheets[0]
                sheet = book.create_sheet(title=key)

                for rowx, row in enumerate(val):
                    for colx, value in enumerate(row):
                        cell = sheet.cell(row=rowx + 1, column=colx + 1)
                        cell.value = value

        timestamp = int(round(time.time() * 10000000))
        file_name = '{0}.xlsx'.format(timestamp)
        file_path = os.path.join(settings.MEDIA_ROOT, file_name)
        book.save(file_path)

        self.file_name = file_name


def fetch_resources3(uri, rel):
    if settings.STATIC_URL and uri.startswith(settings.STATIC_URL):
        path = os.path.join(settings.STATICFILES_DIRS[0], uri.replace(settings.STATIC_URL, ""))
    elif settings.MEDIA_URL and uri.startswith(settings.MEDIA_URL):
        path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
    else:
        path = os.path.join(settings.STATICFILES_DIRS[0], uri)

    if not os.path.isfile(path):
        raise UnsupportedMediaPathException(
            "media urls must start with {} or {}".format(
                settings.MEDIA_ROOT, settings.STATICFILES_DIRS[0]
            )
        )
    return path.replace("\\", "/")

def link_callback(uri, rel):
    """
    Convert HTML URIs to absolute system paths so xhtml2pdf can access those
    resources
    """
    # use short variable names
    sUrl = settings.STATIC_URL      # Typically /static/
    sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/
    mUrl = settings.MEDIA_URL       # Typically /static/media/
    mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/

    # convert URIs to absolute system paths
    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))
    else:
        return uri  # handle absolute uri (ie: http://some.tld/foo.png)

    # make sure that file exists
    if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
    return path

def fetch_resources(self, uri, rel=''):
    """"
    Returns absolute path to resources.

    It search for resources in STATIC_ROOT and MEDIA_ROOT, so you may
    need run manage.py collectstatic first.

    :arg uri Resource URL
    """
    if settings.STATIC_URL in uri:
        absolute_path = os.path.join(
            settings.STATIC_ROOT,
            uri.replace(settings.STATIC_URL, "")
        )
    else:
        absolute_path = os.path.join(
            settings.MEDIA_ROOT,
            uri.replace(settings.MEDIA_URL, "")
        )
    logger.debug(absolute_path)
    return absolute_path

def fetch_resources2(uri, rel):
    path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
    return path

def fetch_resources4(uri, rel):
    if os.sep == '\\': # deal with windows and wrong slashes
        uri2 = os.sep.join(uri.split('/'))
    else:# else, just add the untouched path.
       uri2 = uri

    path = '%s%s' % (settings.SITE_ROOT, uri2)
    return path

def link_callback2(uri, rel):
    sUrl = settings.STATIC_URL      # Typically /static/
    sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/
    mUrl = settings.MEDIA_URL       # Typically /static/media/
    mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/
    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))
    else:
        return uri
    if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
    return path

class GeneratePDF:

    @staticmethod
    def export(template, context, using=None, request=None, encoding="utf-8", **kwargs):
        content = loader.render_to_string(template, context, request=request, using=using)
        return html_to_pdf(content, encoding, link_callback=fetch_resources3, **kwargs)


class GeneratePassword:

    def randomString(stringLength=10):
        """Generate a random string of fixed length """
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(stringLength))