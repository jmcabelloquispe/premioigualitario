# coding:utf-8
"""
Validadores de entrada
======================
"""
import os
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.http import HttpResponseForbidden
from django.conf import settings

name_validator = RegexValidator(r'^[a-zA-z \-\'áéíóúÁÉÍÓÚñÑüÜïÏöÖ]+$')
"""
Valida la presencia de caracteres alfabéticos y especiales adecuados para un
nombre.
"""

name_validator_custom = RegexValidator(r'^[a-zA-Z0-9 \-\'áéíóúÁÉÍÓÚñÑüÜïÏöÖ.°,+&/$#$%()]')

numeric_validator = RegexValidator(r'^[0-9]')
"""Valida la presencia de 8 caracteres numéricos."""

phone_validator = RegexValidator(r'^\+?[0-9 -]{6,9}$')
"""
Valida la presencia de 7 a 14 caracteres numéricos con espacios y guiones
(``-``).
"""
alphanumeric_validator = RegexValidator(r'^[ -~áéíóúÁÉÍÓÚñÑüÜ¡¿]+$')

transaction_validator = RegexValidator(r'^[a-zA-Z0-9\-]+$')

alphanumeric_validator_restricted = RegexValidator(r'^[a-zA-Z0-9 áéíóúÁÉÍÓÚñÑüÜïÏöÖ]+$')

"""
Valida la presencia de todos los caracteres imprimibles incluyendo aquellos
usados en el idioma castellano.
"""


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = settings.SUPPORTED_FILE_TYPES
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Formato de archivo inválido.')


def validate_permission(user, permission):
    return HttpResponseForbidden() if not user.has_perm(permission) and not user.is_superuser else None


def check_perms(user, permission, is_superuser):
    has_permission = False
    if user.has_perm(permission):
        if user.is_superuser == is_superuser:
            has_permission = True
    return has_permission
