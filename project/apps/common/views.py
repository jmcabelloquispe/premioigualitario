# coding:utf-8
"""
Vistas comunes
==============
"""

import json

from django.http import HttpResponse
from django.views.generic import View


class JSONView(View):

    def dispatch(self, request, *args, **kwargs):
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed

        data = None

        if request.method == 'POST':
            if not request.body:
                return HttpResponse(status=400)
            try:
                data = json.loads(request.body.decode('utf-8'))
            except Exception as exception:
                print(exception)
                return HttpResponse(status=400)
        response = handler(request, data, *args, **kwargs)
        return HttpResponse(
            content=json.dumps(response),
            content_type='application/json'
        ) if type(response) in (dict, list) else response


class JSONFormView(JSONView):
    form_class = None

    def post(self, request, data, *args, **kwargs):
        form = self.form_class(data=data)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return {'success': False, 'errors': form.errors}

    def form_valid(self, form):
        return {'success': True}
