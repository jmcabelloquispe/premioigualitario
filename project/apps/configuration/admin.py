#!encoding:utf-8
from django.contrib import admin
from configuration.models import News, Page, Features, AwardsGallery, AwardsCategory, Phases, Evaluator


class NewsInline(admin.TabularInline):

    model = News
    extra = 0
    min_num = 1
    max_num = 20


class FeaturesInline(admin.TabularInline):

    model = Features
    extra = 0
    min_num = 1
    max_num = 20


class AwardsGalleryInline(admin.TabularInline):

    model = AwardsGallery
    extra = 0
    min_num = 1
    max_num = 20


class AwardsCategoryInline(admin.TabularInline):

    model = AwardsCategory
    extra = 0
    min_num = 1
    max_num = 20


class PhasesInline(admin.TabularInline):

    model = Phases
    extra = 0
    min_num = 1
    max_num = 20


class EvaluatorInline(admin.TabularInline):

    model = Evaluator
    extra = 0
    min_num = 1
    max_num = 20


class PageAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_at', 'updated_at')
    inlines = (FeaturesInline, AwardsGalleryInline, AwardsCategoryInline, PhasesInline, EvaluatorInline, NewsInline, )
    fieldsets = (
        ('Información de la Página', {
            'fields': ('name',)
        }),
        ('Caracteristicas', {
            'fields': ('title_features', 'description_features',)
        }),
        ('Premiación', {
            'fields': ('title_awards', 'description_awards',)
        }),
        ('Etapas', {
            'fields': ('title_phases',)
        }),
        ('Evaluadores', {
            'fields': ('title_evaluator', 'description_evaluator')
        }),
        ('Datos de la Noticia', {
            'fields': ('title_news', 'description_news',)
        }),
        ('Bases del Concurso', {
            'fields': ('email_legal', 'phone_legal', 'annexed_legal', 'base_legal', 'brochure_legal', 'url_video_legal',
                       'frequent_questions_legal', 'questionary_legal', 'tyc_legal')
        }),
    )

    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     extra_context = {'title': 'Noticia'}
    #     return self.changeform_view(request, object_id, form_url, extra_context)

    # media
    class Media:
        css = {
            "all": ('business/css/page.css',)
        }


admin.site.register(Page, PageAdmin)
