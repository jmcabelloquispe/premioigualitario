from django.apps import AppConfig


class ConfigurationConfig(AppConfig):

    name = u'configuration'
    verbose_name = u'Configuración'
