# Generated by Django 2.1.7 on 2019-12-26 19:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0011_auto_20191226_1403'),
    ]

    operations = [
        migrations.RenameField(
            model_name='page',
            old_name='questionary',
            new_name='questionary_legal',
        ),
        migrations.RenameField(
            model_name='page',
            old_name='tyc',
            new_name='tyc_legal',
        ),
    ]
