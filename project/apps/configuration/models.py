from django.db import models
# Create your models here.


class Page(models.Model):

    name = models.CharField(
        verbose_name=u'Nombre',
        max_length=100,
        blank=False
    )

    title_news = models.CharField(
        verbose_name=u'Título',
        max_length=60,
        blank=False
    )

    description_news = models.CharField(
        verbose_name=u'Descripción',
        max_length=200,
        blank=False
    )

    title_features = models.CharField(
        verbose_name=u'Título',
        max_length=60,
        blank=False
    )

    description_features = models.CharField(
        verbose_name=u'Descripción',
        max_length=200,
        blank=False
    )

    title_awards = models.CharField(
        verbose_name=u'Título',
        max_length=60,
        blank=False
    )

    description_awards = models.CharField(
        verbose_name=u'Descripción',
        max_length=200,
        blank=False
    )

    title_phases = models.CharField(
        verbose_name=u'Título',
        max_length=60,
        blank=False
    )

    title_evaluator = models.CharField(
        verbose_name=u'Título',
        max_length=60,
        blank=False
    )

    description_evaluator = models.CharField(
        verbose_name=u'Descripción',
        max_length=200,
        blank=False
    )

    email_legal = models.CharField(
        verbose_name=u'Email',
        max_length=40,
        blank=False
    )

    phone_legal = models.CharField(
        verbose_name=u'Teléfono',
        max_length=20,
        blank=False
    )

    annexed_legal = models.CharField(
        verbose_name=u'Anexo',
        max_length=20,
        blank=False
    )

    base_legal = models.FileField(
        verbose_name=u'Bases del concurso',
        max_length=255,
        blank=False,
        upload_to='home/'
    )

    brochure_legal = models.FileField(
        verbose_name=u'Brochure',
        max_length=255,
        blank=False,
        upload_to='home/'
    )

    url_video_legal = models.URLField(
        verbose_name=u'URL video campaña',
        max_length=400,
        blank=False,
    )

    frequent_questions_legal = models.FileField(
        verbose_name=u'Preguntas frecuentes',
        max_length=255,
        blank=False,
        upload_to='home/'
    )

    questionary_legal = models.FileField(
        verbose_name=u'Cuestionario',
        max_length=255,
        blank=False,
        upload_to='home/'
    )

    tyc_legal = models.FileField(
        verbose_name=u'Términos y Condiciones',
        max_length=255,
        blank=False,
        upload_to='home/'
    )

    created_at = models.DateTimeField(
        verbose_name=u'Fecha de Creación',
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        verbose_name=u'Fecha de Actualización',
        auto_now=True
    )

    def __str__(self):
        return u'{0}{1}'.format("#", self.id)

    class Meta:
        verbose_name = u'Página'
        verbose_name_plural = u'Páginas'
        ordering = ('id',)


class News(models.Model):

    page = models.ForeignKey(
        Page,
        verbose_name=u'Pagina',
        on_delete=models.CASCADE
    )

    title = models.CharField(
        verbose_name=u'Título',
        max_length=100,
        blank=False
    )

    description = models.CharField(
        verbose_name=u'Descripción',
        max_length=200,
        blank=False
    )

    image = models.ImageField(
        verbose_name=u'Imagen (320 x 160 px)',
        upload_to='home/',
        blank=False,
    )

    url = models.URLField(
        verbose_name=u'URL de la noticía',
        max_length=400,
        blank=False,
    )

    position = models.IntegerField(
        verbose_name=u'Posición',
        blank=False,
        default=0
    )

    def __str__(self):
        return u'{0}{1}'.format("#", self.id)

    class Meta:
        verbose_name = u'Detalle de Noticia'
        verbose_name_plural = u'Detalle de Noticias'
        ordering = ('position',)


class Features(models.Model):

    page = models.ForeignKey(
        Page,
        verbose_name=u'Pagina',
        on_delete=models.CASCADE
    )

    title = models.CharField(
        verbose_name=u'Título',
        max_length=20,
        blank=False
    )

    description = models.CharField(
        verbose_name=u'Descripción',
        max_length=150,
        blank=False
    )

    image = models.ImageField(
        verbose_name=u'Imagen (60 x 60 px)',
        upload_to='home/',
        blank=False,
    )

    position = models.IntegerField(
        verbose_name=u'Posición',
        blank=False,
        default=0
    )

    def __str__(self):
        return u'{0}{1}'.format("#", self.id)

    class Meta:
        verbose_name = u'Detalle de Caracteristica'
        verbose_name_plural = u'Detalle de Caracteristicas'
        ordering = ('position', )


class AwardsGallery(models.Model):

    page = models.ForeignKey(
        Page,
        verbose_name=u'Pagina',
        on_delete=models.CASCADE
    )

    image = models.ImageField(
        verbose_name=u'Imagen (420 x 420 px)',
        upload_to='home/',
        blank=False,
    )

    position = models.IntegerField(
        verbose_name=u'Posición',
        blank=False,
        default=0
    )

    def __str__(self):
        return u'{0}{1}'.format("#", self.id)

    class Meta:
        verbose_name = u'Galería de Premiación'
        verbose_name_plural = u'Galería de Premiación'
        ordering = ('position', )


class AwardsCategory(models.Model):

    page = models.ForeignKey(
        Page,
        verbose_name=u'Pagina',
        on_delete=models.CASCADE
    )

    title = models.CharField(
        verbose_name=u'Título',
        max_length=60,
        blank=False
    )

    description = models.CharField(
        verbose_name=u'Descripción',
        max_length=200,
        blank=False
    )

    position = models.IntegerField(
        verbose_name=u'Posición',
        blank=False,
        default=0
    )

    def __str__(self):
        return u'{0}{1}'.format("#", self.id)

    class Meta:
        verbose_name = u'Categoría de Premiación'
        verbose_name_plural = u'Categorías de Premiación'
        ordering = ('position', )


class Phases(models.Model):

    page = models.ForeignKey(
        Page,
        verbose_name=u'Pagina',
        on_delete=models.CASCADE
    )

    title = models.CharField(
        verbose_name=u'Título',
        max_length=35,
        blank=False
    )

    date = models.CharField(
        verbose_name=u'Fechas',
        max_length=35,
        blank=False
    )

    sub_title = models.CharField(
        verbose_name=u'Sub Título',
        max_length=35,
        blank=False
    )

    description = models.CharField(
        verbose_name=u'Descripción',
        max_length=190,
        blank=False
    )

    image = models.ImageField(
        verbose_name=u'Imagen (300 x 200 px)',
        upload_to='home/',
        blank=False,
    )

    def __str__(self):
        return u'{0}{1}'.format("#", self.id)

    class Meta:
        verbose_name = u'Detalle de Etapa'
        verbose_name_plural = u'Detalle de Etapas'
        ordering = ('id', )


class Evaluator(models.Model):

    page = models.ForeignKey(
        Page,
        verbose_name=u'Pagina',
        on_delete=models.CASCADE
    )

    name = models.CharField(
        verbose_name=u'Título',
        max_length=20,
        blank=False
    )

    image = models.ImageField(
        verbose_name=u'Imagen (250 x 340 px)',
        upload_to='home/',
        blank=False,
    )

    position = models.IntegerField(
        verbose_name=u'Posición',
        blank=False,
        default=0
    )

    def __str__(self):
        return u'{0}{1}'.format("#", self.id)

    class Meta:
        verbose_name = u'Galería de Evaluador'
        verbose_name_plural = u'Galería de Evaluadores'
        ordering = ('id',)
