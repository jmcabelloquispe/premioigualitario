# coding:utf-8
from django import forms


class LoginForm(forms.Form):

    username = forms.CharField(
        required=True
    )

    password = forms.CharField(
        required=True
    )


class ResetPasswordForm(forms.Form):

    email = forms.CharField(
        required=True
    )
