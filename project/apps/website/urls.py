# coding:utf-8
from django.urls import path
from django.contrib.auth import views as auth_views
from website import views

app_name = 'website'

urlpatterns = [
    path('', views.HomeView.as_view(), {'share': '1'}, name='home'),
    path('form/', views.FormCaseView.as_view(),  name='form'),
    path('thanks/', views.ThanksView.as_view(),  name='thanks'),
    path('dashboard/login/', views.LoginView.as_view(), name='login'),
    path('dashboard/preliminary/participant/', views.PreliminaryParticipantView.as_view(),
         name='preliminary_participant'),
    path('dashboard/preliminary/participant/<int:pk>/detail/', views.PreliminaryParticipantDetailView.as_view(),
         name='preliminary_participantdetail'),
    path('dashboard/final/participant/', views.FinalParticipantView.as_view(),
         name='final_participant'),
    path('dashboard/final/participant/<int:pk>/detail/', views.FinalParticipantDetailView.as_view(),
         name='final_participantdetail'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('dashboard/reset/password/', views.ResetPasswordView.as_view(), name='reset_password')
]
