#!encoding:utf- 8
import time

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.views.generic import View
from django.views.generic import TemplateView, FormView
from easy_pdf.views import PDFTemplateView

from business.models import CompanyType, CompanyCategory, CompanyQuestion, Company, CompanyDetail, \
    CompanyDetailEvaluation
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from common.mail.main import send_email, send_simple_email
from common.utils import GeneratePassword
from configuration.models import Page
from website.forms import LoginForm, ResetPasswordForm
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.views.generic import ListView
from django.db.models import Q


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['random'] = int(round(time.time() * 100000))
        context['page'] = Page.objects.all().first()
        return context


class FormCaseView(TemplateView):
    template_name = 'formcase.html'

    def get_context_data(self, **kwargs):
        context = super(FormCaseView, self).get_context_data(**kwargs)
        context['companyquestions'] = CompanyQuestion.objects.all()
        context['companycategories'] = CompanyCategory.objects.all()
        context['companytypes'] = CompanyType.objects.all()
        context['random'] = int(round(time.time() * 100000))
        context['page'] = Page.objects.all().first()
        return context


class ThanksView(TemplateView):
    template_name = 'thanks.html'


class DesignPDFView(PDFTemplateView):
    template_name = 'post_pdf_test.html'
    # base_url = 'file://' + settings.STATIC_ROOT + '/'
    download_filename = 'hello.pdf'

    def get_context_data(self, **kwargs):
        data = {
            "ruc": "Wunderman Thompson",
            "BASE_URL": settings.BASE_URL,
            "STATIC_URL": settings.STATIC_URL,
            "MEDIA_URL": settings.MEDIA_URL,
            'STATIC_ROOT': settings.STATIC_ROOT,
        }
        return super(DesignPDFView, self).get_context_data(
            pagesize='A4',
            title='Hi there!',
            context=data,
            **kwargs
        )


class PreliminaryParticipantView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Company
    permission_required = CompanyDetailEvaluation.PERMISSION_PRELIMINARY
    login_url = 'website:login'
    template_name = 'participant.html'
    paginate_by = 7

    def get_queryset(self):
        queryset = Company.objects.filter(Q(state=Company.SUITABLE) |
                                          Q(state=Company.QUALIFIED) |
                                          Q(state=Company.FINALIST) |
                                          Q(state=Company.WINNER)).order_by('created_at')

        filter_state = self.request.GET.get("state")
        filter_category = self.request.GET.get("category")

        if filter_state:
            if filter_state == Company.QUALIFIED:
                queryset = queryset.filter(Q(state=Company.QUALIFIED) |
                                           Q(state=Company.FINALIST) |
                                           Q(state=Company.WINNER) |
                                           Q(state=Company.SUITABLE,
                                             companydetailevaluation__jury=self.request.user.id)).distinct()\
                    .order_by('created_at')
            else:
                queryset = queryset.filter(state=filter_state).exclude(
                    companydetailevaluation__jury=self.request.user.id).order_by('created_at')
        if filter_category:
            queryset = queryset.filter(companycategories=filter_category).order_by('created_at')

        return queryset

    def get_context_data(self, **kwargs):
        context = super(PreliminaryParticipantView, self).get_context_data(**kwargs)
        context['participants'] = Company.objects.filter(state=Company.SUITABLE).order_by('created_at')
        return context


class PreliminaryParticipantDetailView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'participantdetail.html'
    permission_required = CompanyDetailEvaluation.PERMISSION_PRELIMINARY
    login_url = 'website:login'

    def dispatch(self, request, *args, **kwargs):
        try:
            company = Company.objects.get(pk=kwargs['pk'])
            if company.state == Company.SUITABLE or \
                    company.state == Company.QUALIFIED or \
                    company.state == Company.FINALIST or company.state == Company.WINNER:
                pass
            else:
                return redirect('website:preliminary_participant')
        except Exception as e:
            print(e)
            return redirect('website:preliminary_participant')
        return super(PreliminaryParticipantDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PreliminaryParticipantDetailView, self).get_context_data(**kwargs)
        context['participant'] = Company.objects.get(pk=kwargs['pk'])
        context['participantdetail'] = CompanyDetail.objects.filter(company_id=kwargs['pk'])
        context['participantevaluation'] = CompanyDetailEvaluation.objects.filter(company_id=kwargs['pk'],
                                                                                  jury=self.request.user.id)[:1]
        return context


class FinalParticipantView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Company
    permission_required = CompanyDetailEvaluation.PERMISSION_FINAL
    login_url = 'website:login'
    template_name = 'finalparticipant.html'
    paginate_by = 7

    def get_queryset(self):
        queryset = Company.objects.filter(Q(state=Company.FINALIST) |
                                          Q(state=Company.QUALIFIED_FINALIST) |
                                          Q(state=Company.WINNER)).order_by('created_at')

        filter_state = self.request.GET.get("state")
        filter_category = self.request.GET.get("category")

        if filter_state:
            if filter_state == Company.QUALIFIED_FINALIST:
                queryset = queryset.filter(Q(state=Company.QUALIFIED_FINALIST) |
                                           Q(state=Company.WINNER) |
                                           Q(state=Company.FINALIST,
                                             companydetailevaluation__jury=self.request.user.id,
                                             companydetailevaluation__type=CompanyDetailEvaluation.FINAL)).distinct()\
                    .order_by('created_at')
            else:
                queryset = queryset.filter(state=filter_state).exclude(
                    companydetailevaluation__jury=self.request.user.id,
                    companydetailevaluation__type=CompanyDetailEvaluation.FINAL).order_by('created_at')
        if filter_category:
            queryset = queryset.filter(companycategories=filter_category).order_by('created_at')

        return queryset

    def get_context_data(self, **kwargs):
        context = super(FinalParticipantView, self).get_context_data(**kwargs)
        context['participants'] = Company.objects.filter(state=Company.SUITABLE).order_by('created_at')
        return context


class FinalParticipantDetailView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'finalparticipantdetail.html'
    permission_required = CompanyDetailEvaluation.PERMISSION_FINAL
    login_url = 'website:login'

    def dispatch(self, request, *args, **kwargs):
        try:
            company = Company.objects.get(pk=kwargs['pk'])
            if company.state == Company.QUALIFIED_FINALIST or company.state == Company.FINALIST \
                    or company.state == Company.WINNER:
                pass
            else:
                return redirect('website:final_participant')
        except Exception as e:
            print(e)
            return redirect('website:final_participant')
        return super(FinalParticipantDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FinalParticipantDetailView, self).get_context_data(**kwargs)
        context['participant'] = Company.objects.get(pk=kwargs['pk'])
        context['participantdetail'] = CompanyDetail.objects.filter(company_id=kwargs['pk'])
        context['participantevaluation'] = CompanyDetailEvaluation.objects.filter(company_id=kwargs['pk'],
                                                                              jury=self.request.user.id)[:1]
        return context


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm

    def form_valid(self, form):
        data = form.cleaned_data
        user = authenticate(username=data['username'], password=data['password'])

        if user is not None:
            login(self.request, user)
        else:
            form.add_error('password', 'Datos incorrectos. Intente otra vez.')
            return super(LoginView, self).form_invalid(form)

        # evalua a que grupo pertenece el usuario y redirecciona
        try:
            if user.has_perm(CompanyDetailEvaluation.PERMISSION_PRELIMINARY):
                return redirect('website:preliminary_participant')
            elif user.has_perm(CompanyDetailEvaluation.PERMISSION_FINAL):
                return redirect('website:final_participant')
            else:
                form.add_error('password', 'Usted no cuenta con los permisos requeridos')
                return super(LoginView, self).form_invalid(form)
        except Exception as e:
            form.add_error('password', 'Usted no cuenta con los permisos requeridos')
            return super(LoginView, self).form_invalid(form)

    def form_invalid(self, form):
        return super(LoginView, self).form_invalid(form)


class LogoutView(View):

    def get(self, request, *args, **kwargs):
        logout(self.request)
        return redirect('website:login')


class ResetPasswordView(FormView):
    template_name = 'resetpassword.html'
    form_class = ResetPasswordForm

    def form_valid(self, form):
        data = form.cleaned_data

        try:
            password = GeneratePassword.randomString()
            user = User.objects.get(email=data['email'])
            user.set_password(password)
            user.save()

            data = {
                'name': user.get_full_name(),
                'password': password,
            }
            send_simple_email(settings.FROM_EMAIL, user.email, settings.SUBJECT_RESET_PASSWORD,
                              'mailing_resetpassword.html', data=data)
            context = {'msg': 'En unos momentos le llegará su nueva contraseña a su correo personal.'}
            return self.render_to_response(context)
        except Exception as e:
            form.add_error('email', 'Correo electronico incorrecto')
            return super(ResetPasswordView, self).form_invalid(form)

    def form_invalid(self, form):
        return super(ResetPasswordView, self).form_invalid(form)
