# coding:utf-8
from settings import *
import os

ALLOWED_HOSTS = ['*']

PG_DB = 'db_premioigualitario'
PG_USER = ''
PG_PASSWORD = ''
PG_HOST = ''
PG_PORT = 5432

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': PG_DB,
        'USER': PG_USER,
        'PASSWORD': PG_PASSWORD,
        'HOST': PG_HOST,
        'PORT': PG_PORT,
    }
}

DEBUG = True

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    )
}

INTERNAL_IPS = ['127.0.0.1']

BASE_URL = 'http://127.0.0.1:8000'

# SSL Configuration
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'http')
SECURE_SSL_REDIRECT = False
CSRF_COOKIE_SECURE = False

