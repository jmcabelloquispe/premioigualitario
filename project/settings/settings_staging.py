# coding:utf-8
from settings import *

DEBUG = True

ALLOWED_HOSTS = ['']

PG_DATABASE = 'db_premioigualitario'
PG_USER = ''
PG_PASSWORD = ''
PG_HOST = ''
PG_PORT = '5432'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': PG_DATABASE,
        'USER': PG_USER,
        'PASSWORD': PG_PASSWORD,
        'HOST': PG_HOST,
        'PORT': PG_PORT
    },
}

BASE_URL = ''
FACEBOOK_ID = ''
EMBLUE_TOKEN = '='

DATA_UPLOAD_MAX_NUMBER_FIELDS = 200000000
DATA_UPLOAD_MAX_MEMORY_SIZE = 200000000
FILE_UPLOAD_MAX_MEMORY_SIZE = 200000000