#!encoding:utf-8

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

admin.site.site_header = 'Administración de Premio Igualitario'
admin.site.site_title = 'Sitio de Administración Premio Igualitario'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api/business/', include('business.urls', namespace="business")),
    path('', include('website.urls', namespace="website")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
