/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	// Componentes
	var Feature = __webpack_require__(1);
	var FormCase = __webpack_require__(2);
	var Participant = __webpack_require__(3);
	var ParticipantDetail = __webpack_require__(4);
	var FinalParticipant = __webpack_require__(5);
	var FinalParticipantDetail = __webpack_require__(6);
	var ResetPassword = __webpack_require__(7);
	var Login = __webpack_require__(8);
	var Common = __webpack_require__(9);
	var APP, UTIL;

	APP = {

	    /* Funciones y mÃƒÅ todos que se aplican al site en general */
	    common: {
	        init: function() {
	            new Common();
	        }
	    },

	    home: {
	        init: function() {
	            new Feature();
	        }
	    },

	    formcase: {
	        init: function() {
	            new FormCase();
	        }
	    },

	    participant: {
	        init: function() {
	            new Participant();
	        }
	    },

	    participantdetail: {
	        init: function() {
	            new ParticipantDetail();
	        }
	    },

	    finalparticipant: {
	        init: function() {
	            new FinalParticipant();
	        }
	    },

	    finalparticipantdetail: {
	        init: function() {
	            new FinalParticipantDetail();
	        }
	    },

	    resetpassword: {
	        init: function() {
	            new ResetPassword();
	        }
	    },

	    login: {
	        init: function() {
	            new Login();
	        }
	    }
	};


	/* Ejecutador de metodos segÃºn vista */

	UTIL = {
	    exec: function(controller, action) {
	        var ns = APP;

	        action = (action === undefined ? "init" : action);

	        if (controller !== "" && ns[controller] && typeof ns[controller][action] === "function") {
	            ns[controller][action]();
	        }
	    },

	    init: function() {
	        var controller = document.getElementsByTagName('body')[0].dataset.controller;

	        UTIL.exec("common");
	        UTIL.exec(controller);
	    }
	};

	$(window).on('load', UTIL.init);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	var Feature = function(){
	    this.initialize();
	    this.onPageReady();
	};

	Feature.prototype.initialize = function(){
	    this.handlers();
	};

	Feature.prototype.handlers = function(){

	    var flag = true;
	    
	    $('.js_slick_statistic').slick({
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        arrows: false,
	        dots: true,
	        mobileFirst: true,
	        responsive: [
	            {
	                breakpoint: 980,
	                settings: 'unslick'
	            }
	        ]
	    }); 

	    $(window).on('resize', function() {
	        $('.js_slick_statistic').slick('resize');
	    });

	    $('.js_slick_news').slick({
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        arrows: false,
	        dots: true,
	        responsive: [           
	            {
	              breakpoint: 1024,
	              settings: {
	                slidesToShow: 1,
	                slidesToScroll: 1
	              }
	            }
	          ]
	    });

	    $('.js_slick_committee').slick({
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        arrows: false,
	        dots: true,
	        responsive: [           
	            {
	              breakpoint: 1024,
	              settings: {
	                slidesToShow: 1,
	                slidesToScroll: 1
	              }
	            }
	          ]
	    });

	    $('.js_slick_evaluator').slick({
	      slidesToShow: 3,
	      slidesToScroll: 3,
	      arrows: false,
	      dots: true,
	      responsive: [           
	          {
	            breakpoint: 769,
	            settings: {
	              slidesToShow: 2,
	              slidesToScroll: 2
	            }
	          },
	          {
	            breakpoint: 480,
	            settings: {
	              slidesToShow: 1,
	              slidesToScroll: 1
	            }
	          }
	        ]
	    });

	    $('.js_slick_galery').slick({
	      slidesToShow: 1,
	      slidesToScroll: 1,
	      arrows: false,
	      dots: true
	    });

	    $('.js_option_tab').on("click", function(){
	      $('.js_option_tab').removeClass('option--active');
	      $(this).addClass('option--active');
	      $('.js_committee, .js_evaluator').css("display", "none");
	      var tab = "."+$(this).data("tab");
	      var carousel = "."+$(this).data("carousel");   
	      $('.js_tab_one, .js_tab_two').removeClass('tab--active');
	      $(tab).addClass('tab--active');
	      $(carousel).slick('setPosition', 0);
	      $(carousel).slick("refresh");
	      var itemPos = $(this).position();
	      $(".tabs_options .selector").css({
	        "left":itemPos.left + "px", 
	      });
	    });

	    $('.js_open_modal').on('click', function(){
	      Swal.fire({
	        title: $(this).data("title"),
	        text: $(this).data("message"),
	        showConfirmButton: false,
	        showCloseButton: true,
	      })
	    });

	    // hace scroll hasta la seccion selecionada en version desktop
	    $('.js_option_header').on('click', function(){
	      var section = "#"+$(this).data('section');
	      // window.location.hash = section;
	      $('html, body').animate({
	        scrollTop: $(section).offset().top - 77
	      }, 1000)
	    });
	    
	    // despliega o oculta menu en version mobile
	    $('.js_hamburger').on('click', function(){                 
	      document.body.classList.toggle('withoutScroll');
	      if(!flag){        
	        $('.js_menu_mobile').removeClass('menu_mobile--show');          
	        $(this).removeClass('rotate');  
	        $('.js_menu_opacity').removeClass('menu_opacity--show');              
	        flag = true;
	      }else {               
	        $('.js_menu_mobile').addClass('menu_mobile--show');              
	        $(this).addClass('rotate');  
	        $('.js_menu_opacity').addClass('menu_opacity--show'); 
	        flag = false;
	      }
	    });

	    $('.js_option_header_mobile').on('click', function(){
	      document.body.classList.toggle('withoutScroll');    
	      var section = "#"+$(this).data('section');
	      $('.js_menu_mobile').removeClass('menu_mobile--show');          
	      $('.js_hamburger').removeClass('rotate');  
	      $('.js_menu_opacity').removeClass('menu_opacity--show');
	      // window.location.hash = section;
	      $('html, body').animate({
	        scrollTop: $(section).offset().top - 70
	      }, 1000)                
	      flag = true;
	    });

	    $('.js_arrow_header_mobile').on('click', function(){
	      var section = "#"+$(this).data('section');
	      $('.js_menu_mobile').removeClass('menu_mobile--show');
	      $('.js_hamburger').removeClass('rotate');
	      $('.js_menu_opacity').removeClass('menu_opacity--show');
	      // window.location.hash = section;
	      $('html, body').animate({
	        scrollTop: $(section).offset().top - 70
	      }, 1000)
	      flag = true;
	    });

	    $(document).scroll(function() {
	      var y = $(this).scrollTop();
	      if (y > 30) {
	        $('.js_header').addClass('header-fixed');
	      } else {
	        $('.js_header').removeClass('header-fixed');
	      }
	    });

	    if(window.location.hash){
	      if (window.innerWidth <= 768) {
	        $('html, body').animate({
	          scrollTop: $(window.location.hash).offset().top - 70
	        }, 1000) 
	      }else {
	        $('html, body').animate({
	          scrollTop: $(window.location.hash).offset().top
	        }, 1000)
	      }     
	    }
	};

	Feature.prototype.onPageReady = function(){
	    $('#loader').fadeOut('slow', function(e){
	        $(this).remove();
	    });
	};

	module.exports = Feature;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	var FormCase = function(){
	  this.form;
	  this.initialize();
	  this.onPageReady();
	};

	Array.prototype.unique=function(a){
	  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
	});

	FormCase.prototype.initialize = function(){
	  var self = this;
	  self.form = $("#form_case");
	  self.validate();
	  self.handlers();
	  self.inputrules();
	};

	FormCase.prototype.validate = function(){
	  var self  = this;
	  self.form.validate({    
	    debug: false,
	    errorClass: 'form-error',
	    messages: {
	      name: {
	        required: "Campo requerido",
	        minlength: "El texto ingresado no es válido",
	        maxlength: "El texto ingresado no es válido",
	      },
	      ruc: {
	        required: "Campo requerido",
	        minlength: "El RUC ingresado no es válido",
	        existruc: "El RUC ya se encuentra registrado en todas las categorías"
	      },
	      representative: {
	        required: "Campo requerido",
	        minlength: "El texto ingresado no es válido",
	        maxlength: "El texto ingresado no es válido",
	      },
	      email: {
	        required: "Campo requerido",
	        minlength: "El correo ingresado no es válido",
	        maxlength: "El correo ingresado no es válido",
	        validEmail: "El correo ingresado no es válido",
	      },
	      phone: {
	        required: "Campo requerido",
	        minlength: "El teléfono ingresado no es válido",
	        maxlength: "El teléfono ingresado no es válido",
	      },
	      terms: {
	        required: "Debe aceptar los términos y condiciones",
	      },
	      videourl: {
	        required: "Campo requerido",
	        url: "La url ingresada no es válida",
	      },
	      pdf:{
	        required: 'Campo requerido',
	        filesize: 'El pdf ingresado excede 5MB',
	        extension: 'El pdf ingresado no es válido'
	      },
	      photo:{
	        required: 'Campo requerido',
	        filesize: 'La foto ingresada excede 5MB',
	        extension: 'La foto ingresada no es válido'
	      },
	      initiative: {
	        required: "Campo requerido",
	        maxlength: "El texto ingresado no es válido",
	      },
	      verifycheck: {
	        required: "Campo requerido",
	        minlength: "Campo requerido",
	        repeatedcategory: "Ya te encuentras registrado en la categoría",
	        exceedlimit: "El RUC ingresado ya excedió el numero máximo de categorías"
	      }
	    },
	    rules: {
	      name: {
	        required: true,
	        minlength: 2,
	        maxlength: 120,
	      },
	      ruc: {
	        required: true,
	        minlength: 11,
	        existruc: true
	      },
	      representative: {
	        required: true,
	        minlength: 2,
	        maxlength: 120,
	      },
	      email: {
	        required: true,
	        minlength: 6,
	        maxlength: 120,
	        // email: true,
	        validEmail:true,
	      },
	      phone: {
	        required: true,
	        minlength: 7,
	        maxlength: 9,
	      },
	      terms:{
	        required: function(){
	          $(".js_terms").is(':checked')
	        }
	      },
	      videourl: {
	        required: false,
	        maxlength: 200,
	        url: true
	      },
	      pdf:{
	        required: false,        
	        extension: "pdf",
	        filesize: 5000000,
	      },
	      photo:{
	        required: false,        
	        extension: "jpg|jpeg|png",
	        filesize: 5000000,
	      },
	      initiative: {
	        required: true,
	        maxlength: 250,
	      },
	      verifycheck: {
	        required: true,
	        minlength: 1,
	        exceedlimit: true,
	        repeatedcategory: true
	      }
	    },
	    errorPlacement: function (error, element) {
	      if(element.hasClass('js_terms')){
	        error.insertAfter(element.parent().parent());
	      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
	        error.insertAfter(element.parent());
	      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
	        error.insertAfter(element.parent());
	      }
	      else {
	        error.insertAfter(element);
	      }
	    },
	    highlight: function(element, errorClass, validClass) {
	      element = $(element);
	      if(element.hasClass('js_terms')){
	        element.parent().addClass('form-error');
	        // element.parent().next().addClass('label-error');
	      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
	        element.parent().addClass('form-error');
	      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
	        element.parent().addClass('form-error');
	      }
	      else {
	        element.addClass('form-error');
	      }
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      element = $(element);
	      if(element.hasClass('js_terms')){
	        element.parent().removeClass('form-error');
	        // element.parent().next().removeClass('label-error');
	      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
	        element.parent().removeClass('form-error');
	      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
	        element.parent().removeClass('form-error');
	      }
	      else {
	        element.removeClass('form-error');
	      }
	    }
	  });
	}

	FormCase.prototype.handlers = function(){
	    var self  = this;
	    // configuracion formulario de pasos del formulario
	    self.form.steps({
	      headerTag: "h3",
	      bodyTag: "section",
	      transitionEffect: "fade",
	      autoFocus: true,
	      labels: {
	        prev: false,
	        next: "Continuar",
	        finish: "Enviar postulación"
	      },
	      onStepChanging: function (event, currentIndex, newIndex)
	      {  
	        if (currentIndex > newIndex)
	        {
	            return true;
	        }
	        self.form.validate().settings.ignore = ":disabled,:hidden";
	        var isValid = self.form.valid();
	        if(isValid){
	          var formcase = document.getElementById('form_case');
	          var formData = new FormData(formcase);
	          fetch('/api/business/company/', {
	            method: 'post',
	            body: formData,
	          }).then(response => {
	            if (!response.ok) {
	              throw new Error(response.statusText)
	            }
	            return response.json()
	          })
	          .catch(error => {
	            Swal.showValidationMessage(
	              `Request failed: ${error}`
	            )
	          });
	          // actualiza el estado de las opciones del menu
	          if(newIndex===0){
	            $('.js_step_next').removeClass('option--active');
	            $('.js_step_previous').addClass('option--active');
	          }else {
	            $('.js_step_next').addClass('option--active');
	            $('.js_step_previous').removeClass('option--active');
	          }
	        }
	        return isValid;
	      },
	      onFinished: function (event, currentIndex)
	      { 
	        self.form.validate().settings.ignore = ":disabled";
	        var isValid = self.form.valid();
	        if(isValid){
	          var formcase = document.getElementById('form_case');
	          var formData = new FormData(formcase);
	          // se agrega las preguntas al formdata
	          $('.js_companyquestion_active').each(function(index, element){
	            formData.append("questions["+index+"]question", $(this).data('companyquestion'));
	            formData.append("questions["+index+"]response", $(this).val());
	          });
	          // muestra alerta antes de hacer el post
	          Swal.fire({
	            type: "warning",
	            title: "Importante",
	            html:"<p>Revisar la información antes de enviar</p>",
	            showConfirmButton: true,
	            confirmButtonText: "Continuar",
	            customClass: "swal2-popup--medium",
	            showCloseButton: true,
	            showLoaderOnConfirm: true,
	            preConfirm: (login) => {
	              return fetch('/api/business/company/step/two/', {
	                  method: 'post',
	                  body: formData,
	              }).then(response => {
	                if (!response.ok) {
	                  throw new Error(response.statusText)
	                }
	                return response.json()
	              })
	              .catch(error => {
	                Swal.showValidationMessage(
	                  `Request failed: ${error}`
	                )
	              })
	            },
	            allowOutsideClick: () => !swal.isLoading()
	          }).then((result) => {
	            if (result.value) {
	              window.location.href="/thanks";
	            }
	          })
	        }
	        return isValid;
	      }
	    });

	    // ejecuta el paso anterior en el formulario
	    $('.js_step_previous').on('click', function(){      
	      var current = self.form.steps("previous");
	      if(current){
	        $('.js_step_next').removeClass('option--active');
	        $(this).addClass('option--active');
	      }
	    });

	    // ejecuta el paso siguiente en el formulario
	    $('.js_step_next').on('click', function(){      
	      var current = self.form.steps("next");
	      if(current){
	        $('.js_step_previous').removeClass('option--active');
	        $(this).addClass('option--active');
	      }
	    });

	    // se aplica jcf a los inputs checkbox y file
	    jcf.replaceAll();

	    // contador de caracteres por input textarea
	    $('textarea').on('keyup', function() {
	       $(this).parent().find('.count .number').html(700 - $(this).val().length) ;
	    });

	    // agregar nuevo input url de video
	    $(document).on('click', '.js_add_input_url', function(){
	      var content = $("body").find(".js_input_url").parent().clone();
	      content.find("input").removeClass("js_input_url");      
	      content.find("input").removeClass("form-error");  
	      content.find("label").remove();
	      content.find("input").addClass("js_input_url_clone"); 
	      content.find("input").attr('name', 'videourl_additional'); 
	      content.find("input").attr('id', 'videourl_additional');
	      content.find("input").val('');
	      content.insertAfter($(".js_input_url").parent());
	      $(content.find("input")).rules("add", {
	        required: true,
	        url: true,
	        messages: {
	          required: "Campo requerido",
	          url: "La url ingresada no es válida"
	        }
	      });
	      $(this).removeClass("js_add_input_url").addClass('js_remove_input_url');
	      $(this).removeClass("button--add").addClass('button--remove');
	      $(this).find('.text').text('Quitar segunda URL');
	   });

	    // eliminar input url de video
	    $(document).on('click', '.js_remove_input_url', function(){
	      $("body").find(".js_input_url_clone").parent().remove();
	      $(this).removeClass("js_remove_input_url").addClass('js_add_input_url');
	      $(this).removeClass("button--remove").addClass('button--add');
	      $(this).find('.text').text('Añadir otra URL de video');
	    });

	     // agregar nuevo input url de pdf
	     $(document).on('click', '.js_add_input_pdf', function(){
	      var content = $("body").find(".js_input_pdf").closest(".content_input").clone();
	      content.append(content.find('input'));
	      content.find('.jcf-file').remove();
	      content.find("label").remove();
	      content.find('input').removeClass("js_input_pdf"); 
	      content.find("input").removeClass("form-error");  
	      content.find("input").addClass("js_input_pdf_clone"); 
	      content.find("input").attr('name', 'pdf_additional');
	      content.find("input").attr('id', 'pdf_additional');
	      content.find("input").val('');
	      content.insertAfter($(".js_input_pdf").closest(".content_input"));
	      $(this).removeClass("js_add_input_pdf").addClass('js_remove_input_pdf');
	      $(this).removeClass("button--add").addClass('button--remove');
	      $(this).find('.text').text('Quitar segundo PDF');
	      jcf.replaceAll();      
	      $(content.find("input")).rules("add", {
	        required: true,
	        filesize: 5000000,
	        extension: "pdf",
	        messages: {
	          required: 'Campo requerido',
	          filesize: 'El pdf ingresado excede 5MB',
	          extension: 'El pdf ingresado no es válido'
	        }
	      });
	    });

	    // quitar input url de pdf
	    $(document).on('click', '.js_remove_input_pdf', function(){
	      var input = $("body").find(".js_input_pdf_clone").closest(".content_input").remove();
	      $(this).removeClass("js_remove_input_pdf").addClass('js_add_input_pdf');
	      $(this).removeClass("button--remove").addClass('button--add');
	      $(this).find('.text').text('Añadir otro archivo PDF');
	    });

	    // agregar nuevo input url de pdf
	    $(document).on('click', '.js_add_input_photo', function(){
	      var content = $("body").find(".js_input_photo").closest(".content_input").clone();
	      content.append(content.find('input'));
	      content.find('.jcf-file').remove();
	      content.find("label").remove();
	      content.find('input').removeClass("js_input_photo"); 
	      content.find('input').removeClass("form-error");   
	      content.find('input').addClass("js_input_photo_clone"); 
	      content.find('input').attr('name', 'photo_additional');
	      content.find('input').attr('id', 'photo_additional');
	      content.find("input").val('');
	      content.insertAfter($(".js_input_photo").closest(".content_input"));
	      $(this).removeClass("js_add_input_photo").addClass('js_remove_input_photo');
	      $(this).removeClass("button--add").addClass('button--remove');
	      $(this).find('.text').text('Quitar segunda foto');
	      jcf.replaceAll();
	      $(content.find("input")).rules("add", {
	        required: true,
	        filesize: 5000000,
	        extension: "jpg|jpeg|png",
	        messages: {
	          required: 'Campo requerido',
	          filesize: 'La foto ingresada excede 5MB',
	          extension: 'La foto ingresada no es válido'
	        }
	      });
	    });

	    // quitar input url de pdf
	    $(document).on('click', '.js_remove_input_photo', function(){
	      var input = $("body").find(".js_input_photo_clone").parent().remove();
	      $(this).removeClass("js_remove_input_photo").addClass('js_add_input_photo');
	      $(this).removeClass("button--remove").addClass('button--add');
	      $(this).find('.text').text('Añadir otra foto');
	    });

	    // se ejecuta cuando el input select companytype hace alguna seleccion
	    $('.js_companytype').on('change', function(){
	      setCategories();
	    });

	    // se ejecuta cuando el input checkbox hace el evento change
	    $('.js_companycategory').on('change', function(){
	      setQuestions();
	      setVerifycheck();
	    });

	    // verifica si por lo menos hay un checkbox seleccionado.
	    function setVerifycheck(){
	      $(".js_verifycheck").val("");
	      var list_id = "";
	      $('.js_companycategory').each(function(){
	        if($(this).is(":checked")){
	          list_id += $(this).val();
	          $(".js_verifycheck").val(list_id);
	        }
	      })      
	      $(".js_verifycheck").focus();
	      $(".js_verifycheck").val(list_id);
	      $(".js_verifycheck").focusout();
	    }

	    // muestra oculta y desactiva checkox segun el tipo de empresa
	    function setCategories(){
	      // obtener todos los los checkbox con el atributo checked
	      var list_checkbox = $('.js_companycategory');
	      // obtener el option del select
	      var select = $('.js_companytype').find(":selected").val();
	      // se reinician los checkbox
	      list_checkbox.prop('checked', true);
	      list_checkbox.trigger('click');
	      list_checkbox.closest(".step__row").addClass("step__row--hidden");
	      // se recorre todo la lista de checkbox para mostrar solo los 
	      // inputs por su tipo de empresa
	      list_checkbox.each(function(){
	        if($(this).data('companytype')==select){
	            $(this).closest(".step__row").removeClass("step__row--hidden");
	        }
	      });
	      $(".js_verifycheck").val("");
	    }    
	    
	    // muestra oculta las preguntas segun la categoria que se seleccione
	    function setQuestions(){
	      var str_id = "";
	      var list_id = [];
	      // obtener todos los los checkbox con el atributo checked
	      var list_checkbox = $('.js_companycategory:checkbox:checked');  
	      // obtener todos los textarea de preguntas ocultas    
	      var list_textarea = $('.js_companyquestion');
	      // se recorre todo la lista de checkbox para crear una cadena separada por coma
	      // la cual se convertira en una lista
	      list_checkbox.each(function(index, element){
	        if(index===(list_checkbox.length - 1)){
	          str_id = str_id + $(this).data('companyquestions');
	        }else {
	          str_id = str_id + $(this).data('companyquestions')+",";
	        }
	      });
	      // se convierte la cadena en lista
	      list_id = str_id.split(",");
	      // se quita los repetidos
	      list_id = list_id.unique();
	      // ocultamos por cada llamada al metodo las preguntas
	      list_textarea.closest(".step__row").addClass("step__row--hidden");
	      list_textarea.removeClass("js_companyquestion_active");      
	      // recorre todo la lista de textarea, se comprueba que el el data exista en list_id
	      // dependiendo de eso muestra los inputs
	      list_textarea.each(function(){
	        if(list_id.indexOf($(this).data('companyquestion').toString())>=0){
	          $(this).closest(".step__row").removeClass("step__row--hidden");
	          $(this).addClass('js_companyquestion_active');
	        }
	      });
	    }

	    // crea modal inicial del formulario
	    function setInitialModal(){
	      Swal.fire({
	        type: "warning",
	        title: "Importante",
	        html:"<p><b>1.</b> El caso o iniciativa debe ser vigente y tener resultados.</p>"+
	             "<p><b>2.</b> Tener toda la información a ingresar en el formulario: archivos, imágenes o videos.</p>"+
	             "<p><b>3.</b> Una empresa puede participar máximo en dos categorías. </p>"+
	             "<p><b>4.</b> Por favor, verifica la información antes de enviarla.</p>"+ 
	             "<p><b>5.</b> Si tiene alguna duda comunicarse al mail <a href='mailto:contacto@premioigualitario.com'>contacto@premioigualitario.com</a></p>",
	        showConfirmButton: true,
	        confirmButtonText: "Continuar",
	        customClass: "swal2-popup--medium",
	        showCloseButton: true
	      });
	    }

	    // se ejecuta para que inicialice las categorias del primer option 
	    // que viene por default
	    setCategories();
	    // se ejectuta para mostrar el modal inicial
	    setInitialModal();

	};

	FormCase.prototype.inputrules = function(){

	  // valida los numero enteros, alfanumerico, etc
	  function setInputFilter(textbox, inputFilter) {
	    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
	      textbox.addEventListener(event, function() {
	        if (inputFilter(this.value)) {
	          this.oldValue = this.value;
	          this.oldSelectionStart = this.selectionStart;
	          this.oldSelectionEnd = this.selectionEnd;
	        } else if (this.hasOwnProperty("oldValue")) {
	          this.value = this.oldValue;
	          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
	        }
	      });
	    });
	  }

	  $("#representative").alpha({
	    disallow: '&,;._-!|"·#@$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…'
	  });

	  $("#email, #name, #initiative").alphanum({
	    disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
	    allow: '@._-'
	  });
	  
	  setInputFilter(document.getElementById("ruc"), function(value) {
	    return /^\d*$/.test(value); 
	  });

	  setInputFilter(document.getElementById("phone"), function(value) {
	    return /^\d*$/.test(value); 
	  });

	};

	FormCase.prototype.onPageReady = function(){
	    $('#loader').fadeOut('slow', function(e){
	        $(this).remove();
	    });
	};
	module.exports = FormCase;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	var Participant = function(){
	  this.initialize();
	  this.activeFilter();
	  this.onPageReady();

	};

	Participant.prototype.initialize = function(){
	  var self = this;
	  self.handlers();
	};

	Participant.prototype.handlers = function(){

	    // filtra la lista de participantes por estado
	    $('.js_filter_state, js_filter_category').on('click', function(){
	        $('.js_filter_state').removeClass('option--active');
	        $(this).addClass('option--active');
	    });

	    // filtra la lista de participantes por categoria
	    // $('.js_filter_category').on('click', function(){
	    //     $('.js_filter_category').removeClass('option--active');
	    //     $(this).addClass('option--active');
	    // });

	    $('.js_hamburger').on('click', function () {
	        if($(this).hasClass('active')){
	            $(this).removeClass('active');
	            $('.js_menu').css('display', 'none');
	        }else {
	            $(this).addClass('active');
	            $('.js_menu').css('display', 'flex');
	        }
	    });

	    $('.js_select_state, .js_select_category').on('change', function () {
	        window.location.search = $(this).find(':selected').data('href');
	    })
	};

	Participant.prototype.activeFilter = function(){
	    var url_string = window.location.href;
	    var url = new URL(url_string);
	    var state = url.searchParams.get("state");
	    var category = url.searchParams.get("category");

	    if(state){
	        $('.js_filter_state').removeClass('option--active');
	        $(".js_filter_state[data-state='"+ state +"']").addClass('option--active');
	        $(".js_select_state").val(state);
	        $('.js_filter_category').each(function() {
	            $(this).attr("href","?state="+state+"&category="+$(this).data('category'));
	        });
	        $('.js_select_category > option').each(function () {
	            $(this).data("href","?state="+state+"&category="+$(this).val());
	        });
	    }

	    if(category){
	        $('.js_filter_category').removeClass('option--active');
	        $(".js_filter_category[data-category='"+ category +"']").addClass('option--active');
	        $(".js_select_category").val(category);
	        $('.js_filter_state').each(function() {
	            $(this).attr("href","?category="+category+"&state="+$(this).data('state'));
	        });
	        $('.js_select_state > option').each(function() {
	            $(this).data("href","?category="+category+"&state="+$(this).val());
	        });
	    }
	};

	Participant.prototype.onPageReady = function(){
	    $('#loader').fadeOut('slow', function(e){
	        $(this).remove();
	    });
	};
	module.exports = Participant;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	var ParticipantDetail = function(){
	  this.form;
	  this.initialize();
	  this.onPageReady();
	};

	ParticipantDetail.prototype.initialize = function(){
	  var self = this;
	  self.form = $("#form_participant_evaluation");
	  self.validate();
	  self.handlers();
	  self.inputrules();
	};

	ParticipantDetail.prototype.validate = function(){
	  var self  = this;
	  self.form.validate({
	    debug: false,
	    errorClass: 'form-error',
	    messages: {
	      comment: {
	        required: "Campo requerido",
	        minlength: "El texto ingresado no es válido",
	        maxlength: "El texto ingresado no es válido",
	      }
	    },
	    rules: {
	      comment: {
	        required: true,
	        minlength: 2,
	        maxlength: 250,
	      }
	    },
	    errorPlacement: function (error, element) {
	        error.insertAfter(element);
	    },
	    highlight: function(element, errorClass, validClass) {
	      element = $(element);
	      element.addClass('form-error');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      element = $(element);
	      element.removeClass('form-error');
	    }
	  });
	};

	ParticipantDetail.prototype.handlers = function(){

	    var self  = this;
	    $('.js_range').on('input', function () {
	        $(this).parent().find('.js_point').html($(this).val());
	    });

	    $("#send_form").on('click', function () {
	        var isValid = self.form.valid();
	        if(isValid){
	            var formcase = document.getElementById('form_participant_evaluation');
	          var formData = new FormData(formcase);
	          Swal.fire({
	            type: "warning",
	            title: "Importante",
	            html:"<p>Revisar la información antes de enviar</p>",
	            showConfirmButton: true,
	            confirmButtonText: "Continuar",
	            customClass: "swal2-popup--medium",
	            showCloseButton: true,
	            showLoaderOnConfirm: true,
	            preConfirm: (login) => {
	              return fetch('/api/business/company/preliminary/evaluation/', {
	                  method: 'post',
	                  body: formData,
	              }).then(response => {
	                if (!response.ok) {
	                  throw new Error(response.statusText)
	                }
	                return response.json()
	              })
	              .catch(error => {
	                Swal.showValidationMessage(
	                  `Request failed: ${error}`
	                )
	              })
	            },
	            allowOutsideClick: () => !swal.isLoading()
	          }).then((result) => {
	            if (result.value) {
	                window.location.href="/dashboard/preliminary/participant/";
	            }
	          })
	        }
	    });

	    $('.js_hamburger').on('click', function () {
	        if($(this).hasClass('active')){
	            $(this).removeClass('active');
	            $('.js_menu').css('display', 'none');
	        }else {
	            $(this).addClass('active');
	            $('.js_menu').css('display', 'flex');
	        }
	    });
	};

	ParticipantDetail.prototype.inputrules = function(){

	  $("#comment").alphanum({
	    disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
	    allow: '@._-'
	  });

	};

	ParticipantDetail.prototype.onPageReady = function(){
	    $('#loader').fadeOut('slow', function(e){
	        $(this).remove();
	    });
	};
	module.exports = ParticipantDetail;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	var FinalParticipant = function(){
	  this.initialize();
	  this.activeFilter();
	  this.onPageReady();

	};

	FinalParticipant.prototype.initialize = function(){
	  var self = this;
	  self.handlers();
	};

	FinalParticipant.prototype.handlers = function(){

	    // filtra la lista de participantes por estado
	    $('.js_filter_state, js_filter_category').on('click', function(){
	        $('.js_filter_state').removeClass('option--active');
	        $(this).addClass('option--active');
	    });

	    $('.js_hamburger').on('click', function () {
	        if($(this).hasClass('active')){
	            $(this).removeClass('active');
	            $('.js_menu').css('display', 'none');
	        }else {
	            $(this).addClass('active');
	            $('.js_menu').css('display', 'flex');
	        }
	    });

	    $('.js_select_state, .js_select_category').on('change', function () {
	        window.location.search = $(this).find(':selected').data('href');
	    })
	};

	FinalParticipant.prototype.activeFilter = function(){
	    var url_string = window.location.href;
	    var url = new URL(url_string);
	    var state = url.searchParams.get("state");
	    var category = url.searchParams.get("category");

	    if(state){
	        $('.js_filter_state').removeClass('option--active');
	        $(".js_filter_state[data-state='"+ state +"']").addClass('option--active');
	        $(".js_select_state").val(state);
	        $('.js_filter_category').each(function() {
	            $(this).attr("href","?state="+state+"&category="+$(this).data('category'));
	        });
	        $('.js_select_category > option').each(function () {
	            $(this).data("href","?state="+state+"&category="+$(this).val());
	        });
	    }

	    if(category){
	        $('.js_filter_category').removeClass('option--active');
	        $(".js_filter_category[data-category='"+ category +"']").addClass('option--active');
	        $(".js_select_category").val(category);
	        $('.js_filter_state').each(function() {
	            $(this).attr("href","?category="+category+"&state="+$(this).data('state'));
	        });
	        $('.js_select_state > option').each(function() {
	            $(this).data("href","?category="+category+"&state="+$(this).val());
	        });
	    }
	};

	FinalParticipant.prototype.onPageReady = function(){
	    $('#loader').fadeOut('slow', function(e){
	        $(this).remove();
	    });
	};
	module.exports = FinalParticipant;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	var FinalParticipantDetail = function(){
	  this.form;
	  this.initialize();
	  this.onPageReady();
	};

	FinalParticipantDetail.prototype.initialize = function(){
	  var self = this;
	  self.form = $("#form_participant_evaluation");
	  self.validate();
	  self.handlers();
	  self.inputrules();
	};

	FinalParticipantDetail.prototype.validate = function(){
	  var self  = this;
	  self.form.validate({
	    debug: false,
	    errorClass: 'form-error',
	    messages: {
	      comment: {
	        required: "Campo requerido",
	        minlength: "El texto ingresado no es válido",
	        maxlength: "El texto ingresado no es válido",
	      }
	    },
	    rules: {
	      comment: {
	        required: true,
	        minlength: 2,
	        maxlength: 250,
	      }
	    },
	    errorPlacement: function (error, element) {
	        error.insertAfter(element);
	    },
	    highlight: function(element, errorClass, validClass) {
	      element = $(element);
	      element.addClass('form-error');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      element = $(element);
	      element.removeClass('form-error');
	    }
	  });
	};

	FinalParticipantDetail.prototype.handlers = function(){

	    var self  = this;
	    $('.js_range').on('input', function () {
	        $(this).parent().find('.js_point').html($(this).val());
	    });

	    $("#send_form").on('click', function () {
	        var isValid = self.form.valid();
	        if(isValid){
	            var formcase = document.getElementById('form_participant_evaluation');
	          var formData = new FormData(formcase);
	          Swal.fire({
	            type: "warning",
	            title: "Importante",
	            html:"<p>Revisar la información antes de enviar</p>",
	            showConfirmButton: true,
	            confirmButtonText: "Continuar",
	            customClass: "swal2-popup--medium",
	            showCloseButton: true,
	            showLoaderOnConfirm: true,
	            preConfirm: (login) => {
	              return fetch('/api/business/company/final/evaluation/', {
	                  method: 'post',
	                  body: formData,
	              }).then(response => {
	                if (!response.ok) {
	                  throw new Error(response.statusText)
	                }
	                return response.json()
	              })
	              .catch(error => {
	                Swal.showValidationMessage(
	                  `Request failed: ${error}`
	                )
	              })
	            },
	            allowOutsideClick: () => !swal.isLoading()
	          }).then((result) => {
	            if (result.value) {
	                window.location.href="/dashboard/final/participant/";
	            }
	          })
	        }
	    });

	    $('.js_hamburger').on('click', function () {
	        if($(this).hasClass('active')){
	            $(this).removeClass('active');
	            $('.js_menu').css('display', 'none');
	        }else {
	            $(this).addClass('active');
	            $('.js_menu').css('display', 'flex');
	        }
	    });
	};

	FinalParticipantDetail.prototype.inputrules = function(){

	  $("#comment").alphanum({
	    disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
	    allow: '@._-'
	  });

	};

	FinalParticipantDetail.prototype.onPageReady = function(){
	    $('#loader').fadeOut('slow', function(e){
	        $(this).remove();
	    });
	};
	module.exports = FinalParticipantDetail;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	var ResetPassword = function(){
	  this.form;
	  this.initialize();
	  this.onPageReady();
	};

	Array.prototype.unique=function(a){
	  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
	});

	ResetPassword.prototype.initialize = function(){
	  var self = this;
	  self.form = $("#form_resetpassword");
	  self.validate();
	  self.handlers();
	  self.inputrules();
	};

	ResetPassword.prototype.validate = function(){
	  var self  = this;
	  self.form.validate({
	    debug: false,
	    errorClass: 'form-error',
	    messages: {
	      email: {
	        required: "Campo requerido",
	        minlength: "El correo ingresado no es válido",
	        maxlength: "El correo ingresado no es válido",
	        validEmail: "El correo ingresado no es válido",
	      }
	    },
	    rules: {
	      email: {
	        required: true,
	        minlength: 6,
	        maxlength: 120,
	        validEmail:true,
	      },
	    },
	    errorPlacement: function (error, element) {
	      if(element.hasClass('js_terms')){
	        error.insertAfter(element.parent().parent());
	      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
	        error.insertAfter(element.parent());
	      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
	        error.insertAfter(element.parent());
	      }
	      else {
	        error.insertAfter(element);
	      }
	    },
	    highlight: function(element, errorClass, validClass) {
	      element = $(element);
	      if(element.hasClass('js_terms')){
	        element.parent().addClass('form-error');
	        // element.parent().next().addClass('label-error');
	      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
	        element.parent().addClass('form-error');
	      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
	        element.parent().addClass('form-error');
	      }
	      else {
	        element.addClass('form-error');
	      }
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      element = $(element);
	      if(element.hasClass('js_terms')){
	        element.parent().removeClass('form-error');
	        // element.parent().next().removeClass('label-error');
	      }else if(element.hasClass('js_input_pdf') || element.hasClass('js_input_pdf_clone')){
	        element.parent().removeClass('form-error');
	      }else if(element.hasClass('js_input_photo') || element.hasClass('js_input_photo_clone')){
	        element.parent().removeClass('form-error');
	      }
	      else {
	        element.removeClass('form-error');
	      }
	    }
	  });
	}

	ResetPassword.prototype.handlers = function(){
	    var self  = this;

	    $("#form_resetpassword").on('submit', function (event) {
	        var isValid = self.form.valid();
	        if(!isValid) {
	            event.preventDefault();
	        }
	    });

	    setTimeout(function () {
	      $(".resetpassword__row--error").css('display', 'none')
	    }, 3000);
	};

	ResetPassword.prototype.inputrules = function(){

	  // valida los numero enteros, alfanumerico, etc
	  function setInputFilter(textbox, inputFilter) {
	    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
	      textbox.addEventListener(event, function() {
	        if (inputFilter(this.value)) {
	          this.oldValue = this.value;
	          this.oldSelectionStart = this.selectionStart;
	          this.oldSelectionEnd = this.selectionEnd;
	        } else if (this.hasOwnProperty("oldValue")) {
	          this.value = this.oldValue;
	          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
	        }
	      });
	    });
	  }


	  $("#email").alphanum({
	    disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
	    allow: '@._-'
	  });

	};

	ResetPassword.prototype.onPageReady = function(){
	    $('#loader').fadeOut('slow', function(e){
	        $(this).remove();
	    });
	};
	module.exports = ResetPassword;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	var Login = function(){
	    this.form;
	    this.initialize();
	    this.onPageReady();
	};

	Login.prototype.initialize = function(){
	  var self = this;
	  self.form = $("#form_login");
	  self.validate();
	  self.handlers();
	};

	Login.prototype.handlers = function(){
	    var self  = this;

	    $("#form_login").on('submit', function (event) {
	        var isValid = self.form.valid();
	        if(!isValid) {
	            event.preventDefault();
	        }
	    });
	};

	Login.prototype.validate = function(){
	  var self  = this;
	  self.form.validate({
	    debug: false,
	    errorClass: 'form-error',
	    messages: {
	      username: {
	        required: "",
	        minlength: "",
	        maxlength: "",
	      },
	      password: {
	        required: "",
	        minlength: "",
	        maxlength: "",
	      }
	    },
	    rules: {
	      username: {
	        required: true,
	        minlength: 2,
	        maxlength: 250,
	      },
	      password: {
	        required: true,
	        minlength: 2,
	        maxlength: 250,
	      }
	    },
	    errorPlacement: function (error, element) {
	        error.insertAfter(element);
	    },
	    highlight: function(element, errorClass, validClass) {
	      element = $(element);
	      element.addClass('form-error');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      element = $(element);
	      element.removeClass('form-error');
	    }
	  });
	};

	Login.prototype.onPageReady = function(){
	    $('#loader').fadeOut('slow', function(e){
	        $(this).remove();
	    });
	};
	module.exports = Login;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	var Common = function(){
	    this.initialize();
	};

	Common.prototype.initialize = function(){
	    this.handlers();
	};

	Common.prototype.handlers = function(){
	    var self = this;
	    $('a.facebook').on('click', function(){
	        ga('send', 'social', 'Facebook', 'share', 'La Hora Inca Kola 2019');
	        FB.ui({
	            method: 'feed',
	            link: 'https://www.facebook.com/IncaKola/videos/300957967260541/',
	            caption: 'DiaInternacionaldelasMujeres'
	        }, function(response){

	        });
	    });

	    $('a.twitter').on('click', function(){
	        ga('send', 'social', 'Twitter', 'tweet', 'La Hora Inca Kola 2019');
	       var link = $(this).data('link');
	       var message = $(this).data('message');
	       var hashtag = 'LaHoraIncaKola,DiaDeLaMujer'
	        uri = 'https://twitter.com/intent/tweet?url=' + link + '&text=' +
	              encodeURI(message) + ' &hashtags=' + hashtag + '&display=popup';
	        window.open(
	            uri,
	            "",
	            "status = 1, height = 450, width = 620, resizable = 0"
	        );
	    });
	};

	module.exports = Common;

/***/ })
/******/ ]);